<?php

/**
 * We need AuthComponent
 */
App::import('component', 'Auth');

/**
 * Class AuthMultipleComponent
 */
class AuthMultipleComponent extends AuthComponent
{
    /**
     * Login redefinition
     *
     * @param   null        $user
     * @return  bool|void
     */
    public function login($user = null)
    {
        /**
         * Parent action returns true
         */
        if (parent::login($user)) {

            $groups = array();

            /**
             * If we have groups
             */
            if (isset($user['Groups']) && null != $user['Groups']) {

                $groups = $user['Groups'];
            }

            $this->Session->write('Auth.User.Groups', $groups);
        }

        return $this->loggedIn();
    }
}