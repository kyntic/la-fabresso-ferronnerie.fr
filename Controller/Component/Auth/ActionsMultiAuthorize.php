<?php

/**
 * Base Authorize
 */
App::uses('BaseAuthorize', 'Controller/Component/Auth');

/**
 * Class ActionsMultiAuthorize
 */
class ActionsMultiAuthorize extends BaseAuthorize
{
    /**
     * Authorize redefinition
     *
     * @param   array       $user
     * @param   CakeRequest $request
     * @return  bool
     */
    public function authorize($user, CakeRequest $request)
    {
        /**
         * Load the ACL Class
         */
        $Acl = $this->_Collection->load('Acl');

        $authorizedGroupCount = 0;

        /**
         * TODO : Assigner un groupe type visiteur de maniere automatique non ?
         */
        if (!isset($user['Groups'])) {

            return false;
        }

        foreach ($user['Groups'] as $group) {

            $check = $Acl->check(
                array(
                    'foreign_key'   => $group['id'],
                    'model'         => $group['model']
                ),
                $this->action($request)
            );

            if ($check) {

                $authorizedGroupCount++;
            }
        }

        /**
         * If at least one group is authorized
         */
        if ($authorizedGroupCount > 0) {

            return true;
        }

        return false;
    }
}