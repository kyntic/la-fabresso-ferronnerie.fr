<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * @information : Pour reinitialiser les aros/acos mettre "return true;" dans isAuthorized
 */

App::uses('Controller', 'Controller');

/**
 * Class AppController
 */
class AppController extends Controller
{
    /**
     * The helpers
     *
     * @var array $helpers
     */
    public $helpers = array(
        'WhPrix',
        'WhDate',
        'WhMenu',
        'Session',
        'Html',
        'Form',
        'WhForm',
        'Js' => array(
            'Jquery'
        ),
        'FileManager.WhFile',
        'Text',
        'Smart'
    );

    /**
     * The components
     *
     * @var array $components
     */
    public $components = array(
        'Acl',
        'Saml.Saml',
        'Ldap.Ldap',
        'Auth' => array(
            'className' => 'AuthMultiple',
            'authorize' => array(
                'ActionsMulti' => array(
                    'actionPath' => 'controllers/'
                )
            ),
            'authenticate' => array('Saml.Saml')
        ),
        'Session',
        'Cookie',
        'RequestHandler',
        'DebugKit.Toolbar'
    );

    /**
     * The content
     *
     * @var array $Cont
     */
    public $Cont;

    /**
     * Breadcrumb
     *
     * @var string $_breadcrumb
     */
    public $_breadcrumb;

    /**
     * Before filter
     *
     * @return void
     */
    public function beforeFilter()
    {
        /**
         * Permet de mettre en place les ACL
         */
        /**$this->Auth->authorize = array(
            'Controller',
            'Actions' => array(
                'actionPath' => 'controllers'
            )
        );**/

        /**
         * Acces a la partie public du site
         */
        /**if ($this->request->here != '/' &&
            !isset($this->request->params['requested'])
        ) {

            $this->Saml->requireAuth();
        }**/

        $this->Saml->requireAuth();

        /**
         * Get the user attributes
         */
        $userSaml = $this->Saml->getAttributes();

        /**
         * We are not logged in
         */
        if (AuthComponent::user() == null &&
            !empty($userSaml)
        ) {
            /**
             * The model we need
             */
            $this->loadModel('Ldap.LdapUser');
            $this->loadModel('UserGroup');

            /**
             * Get the user
             */
            $userLdap = $this->LdapUser->findById($userSaml['uid'][0]);

            /**
             * No user found - Redirect
             */
            if (!$userLdap) {

                $this->Session->setFlash('Utilisateur non trouvé', 'error');
                $this->redirect('/');
            }

            /**
             * Build the user to login
             */
            $user = array(
                'id'        => $userLdap['LdapUser']['id'],
                'name'      => $userLdap['LdapUser']['name'],
                'email'     => $userSaml['mail'][0],
                'dn'        => $userLdap['LdapUser']['dn'],
            );

            /**
             * Add the groups
             */
            if (isset($userLdap['LdapGroups']) &&
                !empty($userLdap['LdapGroups'])
            ) {

                foreach ($userLdap['LdapGroups'] as $group) {

                    $user['Groups'][] = array(
                        'id'    => $group['LdapGroup']['id'],
                        'model' => 'LdapGroup'
                    );
                }
            }

            /**
             * If we have the main group
             */
            if (isset($userLdap['LdapGroup']['id'])) {

                $user['Groups'][] = array(
                    'id'    => $userLdap['LdapGroup']['id'],
                    'model' => 'LdapGroup'
                );
            }

            /**
             * Get the local groups (Local admin for example)
             */
            $localUserGroups = $this->UserGroup->find(
                'all',
                array(
                    'conditions' => array(
                        'UserGroup.user_id' => $user['id']
                    )
                )
            );

            if ($localUserGroups) {

                foreach ($localUserGroups as $userGroup) {

                    $user['Groups'][] = array(
                        'id'    => $userGroup['UserGroup']['group_id'],
                        'model' => 'Group'
                    );
                }
            }

            $this->Auth->login($user);

            if ($this->Auth->loggedIn()) {

                //$this->_loadUserRights();
            }
        }

        if ((isset($this->params['prefix']) && $this->params['prefix'] == 'admin') ||
            $this->action == 'admin_login' ||
            $this->action == 'admin_logout'
        ) {

            /**
             * Fonctions d'administration
             */
            $this->_administration();

        } elseif((isset($this->params['prefix']) && $this->params['prefix'] == 'membre')) {

            /**
             * Fonctions pour les membres
             */
            $this->_membreBeforeFilter();

        } else {

            /**
             * Fonction pour les visiteurs
             */
            $this->_publicBeforeFilter();
        }
    }

    /**
     * Before render
     *
     * @return void
     */
    public function beforeRender()
    {
        if ((isset($this->params['prefix']) &&
            $this->params['prefix'] == 'membre')
        ) {
            $this->_membreBeforeRender();
        }

        $this->_publicBeforeRender();
    }

    /**
     * Is Authorized
     *
     * @return bool
     */
    public function isAuthorized()
    {
        return $this->Auth->loggedIn();
    }

    /**
     * Recupere tous les acos du user
     *
     * TODO : Transformer les acos en url pour checker .... ca va etre de la merdasse !
     *
     * @return bool
     */
    private function _loadUserRights()
    {
        /**
         * Get the user
         */
        $user = AuthComponent::user();

        if ($user == null) {

            return false;
        }

        $groups = array();

        foreach ($user['Groups'] as $group) {

            $groups[] = $group['id'];
        }

        /**
         * Get all the aros
         */
        $aros = $this->Acl->Aro->find(
            'all',
            array(
                'conditions' => array(
                    'Aro.foreign_key' => $groups
                )
            )
        );

        if (!$aros) {

            return false;
        }
    }

    /**
     * Administration
     *
     * @return void
     */
    private function _administration()
    {
        /**
         * Moxie
         */
        $this->Session->write('moxiecodeinit', true);

        /**
         * Menu admin
         */
        $this->loadModel('AdminMenu');
        $this->set('AdminMenu', $this->AdminMenu->find('threaded', array('conditions' => 'AdminMenu.afficher', 'order' => 'AdminMenu.position')));

        $this->helpers['Smart'] = array();

        /**
         * Layout
         */
        $this->layout = $this->params['prefix'] . '_default';
    }

    /**
     * Membre Before filter
     *
     * @return void
     */
    private function _membreBeforeFilter()
    {
        /**
         * The model we need
         */
        $this->loadModel('MembreMenu');

        /**
         * Get the menu
         */
        $membreMenu = $this->MembreMenu->find(
            'threaded',
            array(
                'conditions'    => 'MembreMenu.afficher',
                'order'         => 'MembreMenu.position'
            )
        );

        foreach ($membreMenu as $key => $menu) {

            /**
             * Check if the user has access
             */
            $hasAccess = $this->_checkMenuRights($menu['MembreMenu']['aco_route']);

            if (!$hasAccess) {

                unset($membreMenu[$key]);
            }
        }

        $this->set('MembreMenu', $membreMenu);
        $this->layout = 'membre_default';
    }

    /**
     * Check the rights
     *
     * @param   null    $acoRoute
     * @return  bool
     */
    private function _checkMenuRights($acoRoute = null)
    {
        /**
         * If no aco route
         */
        if ($acoRoute == null) {

            return false;
        }

        /**
         * Get the user groups
         */
        $userGroups = AuthComponent::user('Groups');

        /**
         * If no user groups
         */
        if ($userGroups == null) {

            return false;
        }

        foreach ($userGroups as $group) {

            $hasAccess = $this->Acl->check(
                array(
                    'model'         => $group['model'],
                    'foreign_key'   => $group['id']
                ),
                $acoRoute
            );

            if ($hasAccess) {

                return true;
            }
        }

        return false;
    }

    /**
     * Public Before Filter
     *
     * @return void
     */
    private function _publicBeforeFilter()
    {
        /**
         * Vérfier si il existe un cookie d'autoconnexion
         */
        $UserUuid = $this->Cookie->read('User.uuid');

        if ($UserUuid) {

            $this->loadModel('User');
            $User = $this->User->find('first', array('conditions' => array('User.uuid' => $UserUuid)));
            $this->Auth->login($User['User']);
        }

        if (empty($this->params['prefix'])) {

            $this->Auth->allow();
        }
    }

    /**
     * Public Before Render
     *
     * @return void
     */
    private function _publicBeforeRender()
    {
        $this->loadModel('MenuItem');
        $this->loadModel('ContactManager.Contact');
        $this->loadModel('Parametre');
        $this->loadModel('ContactManager.Contact');

        if (!empty($this->Cont)) {

            $this->set('Cont', $this->Cont);
        }

        $this->helpers['WhTree'] = array();

        $_menuPages = (Configure::read('debug') == 0) ? Cache::read('_menuPages') : false;

        if (!$_menuPages) {

            $_menuPages = $this->MenuItem->find('threaded', array('conditions' => array('MenuItem.menu_id', 'MenuItem.etat_id'), 'order' => 'MenuItem.position'));

            Cache::write('_menuPages', $_menuPages);
        }

        $_parametres = $this->Parametre->find('first');
        $_contactSite = $this->Contact->find('first');

        $this->set(compact('_menuPages', '_contactSite', '_parametres'));
    }

    /**
     * Membre Before Render
     *
     * @return void
     */
    private function _membreBeforeRender()
    {
        $this->loadModel('MenuItem');
        $this->loadModel('ContactManager.Contact');
        $this->loadModel('Parametre');

        if (!empty($this->Cont)) {

            $this->set('Cont', $this->Cont);
        }

        $this->helpers['WhTree'] = array();

        $_menuPages = (Configure::read('debug') == 0) ? Cache::read('_menuPages') : false;

        if (!$_menuPages) {

            $_menuPages = $this->MenuItem->find('threaded', array('conditions' => array('MenuItem.menu_id', 'MenuItem.etat_id'), 'order' => 'MenuItem.position'));

            Cache::write('_menuPages', $_menuPages);
        }

        $_parametres = $this->Parametre->find('first');

        $this->set(compact('_menuPages', '_contactSite', '_parametres'));
    }

    /**
     * Set Error Layout
     *
     * @return void
     */
    function _setErrorLayout()
    {
        if ($this->name == 'CakeError') {

            $this->loadModel('Home');
            $this->Cont = $this->Home->find('first'); //Parce ce que c'est l'accueil, donc une seule
        }
    }
}
