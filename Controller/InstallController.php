<?php

/**
 * Class InstallController
 */
class InstallController extends AppController
{

	/**
	 * Installation d'un site
	 */
	public function index()
	{

		// Récupération des thèmes
        $themes = file_get_contents('http://themes.kobaltis.net');
        $themes = json_decode($themes);
		$this->set('themes', $themes);

        if (!empty($this->request->data)) {

	        $data = $this->request->data;

	        // Installation du thème

	        $theme = $themes[$data['Install']['theme']];
	        $theme->nameCamelCase = Inflector::camelize($theme->name);
	        $themeZipUrl = 'http://themes.kobaltis.net/' . $theme->url;

	        $themeZip = file_get_contents($themeZipUrl);

	        $themesFolder = APP . 'View/Themed/';
	        $themeZipLocalUrl =  $themesFolder . $theme->nameCamelCase . '.zip';

	        // Création du zip
	        file_put_contents($themeZipLocalUrl, $themeZip);

	        // Unzip
	        $zip = new ZipArchive;
	        if ($zip->open($themeZipLocalUrl) === TRUE) {

		        $zip->extractTo(APP . '/View/Themed/');
		        $zip->close();

	        }

	        // Déplacement des assets
	        if (is_dir($themesFolder . $theme->nameCamelCase . '/assets')) {

		        rename($themesFolder . $theme->nameCamelCase . '/assets', APP . 'htdocs/' . strtolower($theme->nameCamelCase));

	        }


	        // Ecriture du core

	        $fileCorePath = APP . 'Config/core.php';

	        $fileCore = file_get_contents($fileCorePath);

	        $fileCore = preg_replace('#__THEME__#',  $theme->nameCamelCase, $fileCore);

	        $handle = fopen($fileCorePath, "w");
	        fwrite($handle, $fileCore);
	        fclose($handle);


	        // Ecriture des paramètres de la BDD

	        $fileDatabasePath = APP . 'Config/database.php';

	        $fileDatabase = file_get_contents($fileDatabasePath);

	        $fileDatabase = preg_replace('#DummySource#', 'Database/Mysql', $fileDatabase);
	        $fileDatabase = preg_replace('#%%%login%%%#', $data['Install']['login'], $fileDatabase);
	        $fileDatabase = preg_replace('#%%%password%%%#', $data['Install']['password'], $fileDatabase);
	        $fileDatabase = preg_replace('#%%%database%%%#', $data['Install']['database'], $fileDatabase);

	        $handle = fopen($fileDatabasePath, "w");
	        fwrite($handle, $fileDatabase);
	        fclose($handle);

            // Installation de la BDD
	        App::uses('ConnectionManager', 'Model');
	        $db = ConnectionManager::getDataSource("default");

            if (!$db->isConnected()) {

                $this->Session->setFlash('error', 'Les paramètres de connexion ne sont pas corrects');
                $this->redirect('/install');

            }

	        $sql = file_get_contents(APP . '/bdo.sql');
			$db->rawQuery($sql);

			// Initialisation des groupes et des utilisateurs par défaut
	        $this->loadModel('Group');
	        $this->loadModel('User');

	        $this->Group->save(
                array(
                    'id'    =>  1,
                    'name'  =>  'Administrateurs'
                )
	        );

	        $this->Group->save(
                array(
                    'id'    =>  2,
                    'name'  =>  'Redacteurs'
                )
	        );

            $this->User->save(
                array(
	                'id'                => null,
                    'email'             => 'jerome@whatson-web.com',
                    'nom'               => 'Lebleu',
                    'prenom'            => 'Jerome',
                    'motdepasse'        => 'admin',
                    'confirm_password'  => 'admin',
                    'group_id'          => 1,
                    'verified'          => 1
                ),
                array(
                    'validate' => false
                )
            );

	        $this->User->save(
               array(
                   'id'                => null,
                   'email'             => 'prod@kobaltis.net',
                   'nom'               => 'Kobaltis',
                   'prenom'            => 'Prod',
                   'motdepasse'        => 'mdpkobaltis2000',
                   'confirm_password'  => 'mdpkobaltis2000',
                   'group_id'          => 1,
                   'verified'          => 1
               ),
               array(
                   'validate' => false
               )
	        );

	        $this->User->save(
               array(
                   'id'                => null,
                   'email'             => 'redacteur@kobaltis.net',
                   'nom'               => 'Kobaltis',
                   'prenom'            => 'Redacteur',
                   'motdepasse'        => 'mdpkobaltis2000',
                   'confirm_password'  => 'mdpkobaltis2000',
                   'group_id'          => 2,
                   'verified'          => 1
               ),
               array(
                   'validate' => false
               )
	        );

            $this->redirect('/admin');

        }

        $this->layout = 'install';

	}

}
