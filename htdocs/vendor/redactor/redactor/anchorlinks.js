if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.anchorlinks = function()
{
    return {
        /**
         * Init function
         */
        init: function()
        {
            /**
             * Add a template
             */
            this.modal.addTemplate('anchor', this.anchorlinks.getTemplate);

            /**
             * Define the button
             */
            var button = this.button.add('anchor', 'Ancre');

            /**
             * Font awesome
             */
            this.button.setAwesome('anchor', 'fa-anchor');

            /**
             * Button callback
             */
            this.button.addCallback(button, this.anchorlinks.show);
        },
        /**
         * Display the plugin
         */
        show : function()
        {
            this.selection.save();

            /**
             * Load the modal
             */
            this.modal.load('anchor', 'Insérer une ancre ...', 400);

            /**
             * Add cancel button
             */
            this.modal.createCancelButton();

            /**
             * Input
             *
             * @type {*|jQuery|HTMLElement}
             */
            var anchorInputName = $('<input type="text" id="redactor-anchorlinks-name" />');

            /**
             * Append to the div
             */
            $('#anchorlinks').append(anchorInputName);

            /**
             * Add submit button
             */
            var submitButton = this.modal.createActionButton('Ajouter');
            var deleteButton = this.modal.createDeleteButton('Supprimer');

            /**
             * Click event
             */
            submitButton.on('click', this.anchorlinks.insertAnchor);
            deleteButton.on('click', this.anchorlinks.deleteAnchor);

            /**
             * Display modal
             */
            this.modal.show();
        },
        /**
         * Insert the anchor
         */
        insertAnchor: function()
        {
            /**
             * Get the anchor name
             *
             * @type {*|jQuery}
             */
            var anchorName = $('#redactor-anchorlinks-name').val();

            /**
             * No anchor name
             */
            if (anchorName == '') {

                alert('Veuillez remplir le nom de l\'ancre.');
                return;
            }

            /**
             * Restore and close
             */
            this.selection.restore();
            this.modal.close();

            /**
             * Set attributes
             */
            this.block.setAttr('id', anchorName);
            this.block.setAttr('class', 'wh-anchor');

            this.code.sync();
        },
        /**
         * Deletes anchor
         */
        deleteAnchor: function()
        {
            this.selection.restore();
            this.modal.close();

            /**
             * Remove attributes
             */
            this.block.removeAttr('name');
            this.block.removeAttr('class');

            this.code.sync();
        },
        /**
         * Get the template
         *
         * @returns {string}
         */
        getTemplate: function()
        {
            return String()
                + '<section id="redactor-modal-anchorlinks-insert">'
                + '<label>Nom de l\'ancre</label>'
                + '<div id=anchorlinks></div>'
                + '</section>';
        }
    };
};
