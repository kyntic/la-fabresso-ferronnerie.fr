if (!RedactorPlugins) var RedactorPlugins = {};

(function($)
{
	RedactorPlugins.definedlinks = function()
	{
		return {
			init: function()
			{
				if (!this.opts.definedLinks) return;

				this.modal.addCallback('link', $.proxy(this.definedlinks.load, this));

			},
			load: function()
			{
                /**
                 * Select links
                 *
                 * @type {*|HTMLElement}
                 */
				var $select = $('<select id="redactor-defined-links" class="chosen-select" />');

                /**
                 * Select anchor from list
                 *
                 * @type {*|HTMLElement}
                 */
                var $selectAnchor = $('<select id="redactor-anchor-links" class="chosen-select" />');

                /**
                 * Add the select
                 */
				$('#redactor-modal-link-insert').prepend($select);
                $('#redactor-modal-link-insert').prepend('<label>Choisir une page</label>');
                $('#redactor-modal-link-insert').prepend($selectAnchor);
                $('#redactor-modal-link-insert').prepend('<label>Choisir une ancre</label>');

                /**
                 * Define storage
                 *
                 * @type {{}}
                 */
                this.definedlinks.storage           = {};
				this.definedlinks.storage.page      = {};
                this.definedlinks.storage.anchor    = {};

				$.getJSON(this.opts.definedLinks, $.proxy(function(data)
				{
					$.each(data, $.proxy(function(key, val)
					{
						this.definedlinks.storage.page[key] = val;
						$select.append($('<option>').val(key).html(val.name));

					}, this));

					$select.on('change', $.proxy(this.definedlinks.select, this));

                    /**
                     * Add select 2
                     */
                    $("#redactor-defined-links").chosen();
                    $("#redactor-anchor-links").chosen();

				}, this));

                /**
                 * Get the anchors
                 *
                 * @type {*|HTMLElement}
                 */
                var anchors = $('.wh-anchor');

                $.each(anchors, $.proxy(function(key, value)
                {
                    /**
                     * Append all the anchors
                     */
                    $selectAnchor.append($('<option>').val(key).html($(value).attr('id')));
                    this.definedlinks.storage.anchor[key] = $(value).attr('id');

                }, this));

                $selectAnchor.on('change', $.proxy(this.definedlinks.selectAnchor, this));
			},
			select: function(e)
			{
				var key = $(e.target).val();
				var name = '', url = '';
				if (key !== 0)
				{
					name    = this.definedlinks.storage.page[key].name;
					url     = this.definedlinks.storage.page[key].url;
				}

				$('#redactor-link-url').val(url);

				var $el = $('#redactor-link-url-text');
				if ($el.val() === '') $el.val(name);
			},
            /**
             * Select the anchor
             *
             * @param e
             */
            selectAnchor: function(e)
            {
                /**
                 * Selected key
                 */
                var key     = $(e.target).val();
                var url     = this.definedlinks.storage.anchor[key];

                $('#redactor-link-url').val('#' + url);
            }
		};
	};
})(jQuery);