if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.mp3 = function()
{
    return {

        getTemplate: function()
        {
            return String()
                + '<section id="redactor-modal-mp3-insert">'
                + '<label>Séléctionnez votre mp3</label>'
                + '<div id="mp3Select"></div>'
                + '</section>';
        },

        init: function()
        {
            this.modal.addTemplate('mp3', this.mp3.getTemplate());

            var button = this.button.add('mp3', 'Mp3');
            this.button.setAwesome('mp3', 'fa-music');
            this.button.addCallback(button, this.mp3.show);
        },

        show: function()
        {
            this.modal.load('mp3', 'Insérer un Mp3', 400);

            this.modal.createCancelButton();

            var $select = $('<select id="redactor-defined-links" />');
            $('#mp3Select').append($select);

            this.mp3.storage = {};

            $.getJSON(this.opts.definedLinks + '/model:file/extention_id:mp3', $.proxy(function(data)
            {
                $.each(data, $.proxy(function(key, val)
                {
                    this.mp3.storage[key] = val;
                    $select.append($('<option>').val(key).html(val.name));

                }, this));

                $select.on('change', $.proxy(this.mp3.select, this));

                //Ajouter select2 :
                $("#redactor-defined-links").chosen();

            }, this));

            //this.selection.save();
            this.modal.show();
        },
        select: function(e)
        {
            var key = $(e.target).val();
            var name = '', url = '';
            if (key !== 0)
            {
                //name    = this.mp3.storage[key].name;
                url     = this.mp3.storage[key].url;
            }

            var lecteur = '';

            lecteur += '<audio controls="controls">';
            lecteur += '<source src="' + url + '" type="audio/mp3" />';
            lecteur += 'Votre navigateur n’est pas compatible';
            lecteur += '</audio>';

            this.selection.restore();
            this.modal.close();

            var current = this.selection.getBlock() || this.selection.getCurrent();

            if (current) $(current).after(lecteur);
            else
            {
                this.insert.html(lecteur);
            }

            this.code.sync();
        }
    };
};

