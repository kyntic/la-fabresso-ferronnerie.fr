(function($){
	$(document).ready(function(){
		$('.business-tablet-element').waypoint(function() {
			$(this).find('.col-md-6:first img').stop(true).delay(600).animate({opacity : 1}, 400, function(){
				$(this).closest('.business-tablet-element').find('.col-md-6:last .small-block-tablet').each(function(index){
					$(this).stop(true).delay(($(this).index())*300).animate({opacity : 1}, 400);
				});
			});
		}, {offset: '100%'});
	});
})(jQuery);
