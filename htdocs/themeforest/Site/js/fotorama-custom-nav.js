(function($){
$(document).ready(function(){
	
	/*  Fotorama Custom Thumbs  */
	var $elementWidth = $('.nav-thumb-wrap').outerWidth();
	var $wrapperWidth = $('.fotorama-nav-wrap-custom').css({'width' : $elementWidth * 3});
	var $thumbsHeight = $('.nav-thumb-wrap').css('height');
	$('.fotorama-nav-wrap-custom').css({'height' : $thumbsHeight});
	
	$('.nav-thumb-wrap').each(function(index){
		$(this).click(function(e){
			if($(this).hasClass('top-element')) {e.preventDefault();}
			$('.fotorama__thumbs-shaft > i').eq(index).trigger('click');
			$(this).closest('.fotorama-nav-wrap-custom').find('.active-thumb').removeClass('active-thumb');
			$(this).addClass('active-thumb');

		var $elementWidth = $('.nav-thumb-wrap').width();
		var $wrapperWidth = $('.fotorama-nav-wrap-custom').width();
		var $elementPosition = $(this).index();
		var $totalElements = $('.nav-thumb-wrap').length;
		var $windWidth = $(window).width();
		if($windWidth > 1180){
			if($elementPosition > 0){
				if($totalElements -1 != $elementPosition ){
					$(this).parent().stop(true).animate({left : - (($elementPosition - 1) * $elementWidth)}, 400);
				}
			}
		}
		if($windWidth < 1180){
			$('.fotorama-nav-wrap-custom').show();
			$('.fotorama-nav-wrap-custom').css({'width' : $elementWidth * 2});
			$('.fotorama-nav-wrap-custom').css({'margin-left' : -$elementWidth});
			var $elementPosition = $(this).index();
				if($totalElements - 1 != $elementPosition ){
					$(this).parent().stop(true).animate({left : - ($elementPosition * $elementWidth)}, 400);
				}
		}
		if($windWidth < 800){
			$('.fotorama-nav-wrap-custom').hide();
		}
		

		});			
	});
	
	
	fotoramaNavReset();
	
});

$(window).resize(function(){
		fotoramaNavReset();
	})
	
	function fotoramaNavReset() {
		var $windWidth = $(window).width();
		var $elementWidth = $('.nav-thumb-wrap:first').width();
		var $this = $('.nav-thumb-wrap.active-thumb');
		var $elementPosition = $('.nav-thumb-wrap.active-thumb').index();
		var $totalElements = $('.nav-thumb-wrap').length;
		if($windWidth > 1180){
			$('.fotorama-nav-wrap-custom').show();
			$('.fotorama-nav-wrap-custom').css({'width' : $elementWidth * 3});
			$('.fotorama-nav-wrap-custom').css({'margin-left' : -1.5*$elementWidth});
			if($elementPosition > 0){
				if($totalElements -1 != $elementPosition ){
					$this.parent().stop(true).css({left : - (($elementPosition - 1) * $elementWidth)});
				}
				else {
					$this.parent().stop(true).css({left : - (($elementPosition - 2) * $elementWidth)});
				}
			}
		}
		if($windWidth < 1180){
			$('.fotorama-nav-wrap-custom').show();
			$('.fotorama-nav-wrap-custom').css({'width' : $elementWidth * 2});
			$('.fotorama-nav-wrap-custom').css({'margin-left' : -$elementWidth});
			if($elementPosition > 0){
				if($totalElements - 1 != $elementPosition ){
					$this.parent().stop(true).css({left : - ($elementPosition * $elementWidth)}, 400);
				}
				else {
					$this.parent().stop(true).css({left : - (($elementPosition-1) * $elementWidth)}, 400);
				}
			}
		}
		if($windWidth < 800){
			$('.fotorama-nav-wrap-custom').hide();
		}
	}
})(jQuery);