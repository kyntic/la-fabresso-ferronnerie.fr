(function($){
	$(document).ready(function(){
		var $windowWidth = $(window).width();
		if($windowWidth > 1180){
			$( "#carousel" ).rcarousel({
				visible: 6,
				step: 1,
				width: 160,
				height: 45,
				speed: 400
			});
		}
		if($windowWidth <= 1180){
			if($windowWidth < 1000){
				if($windowWidth < 690){
					if($windowWidth < 530){
						if($windowWidth < 370){
							$( "#carousel" ).rcarousel({
								visible: 1,
								step: 1,
								width: 160,
								height: 45,
								speed: 400
							});
						}
						$( "#carousel" ).rcarousel({
							visible: 2,
							step: 1,
							width: 160,
							height: 45,
							speed: 400
						});
					}
					$( "#carousel" ).rcarousel({
						visible: 3,
						step: 1,
						width: 160,
						height: 45,
						speed: 400
					});
				}
				$( "#carousel" ).rcarousel({
					visible: 4,
					step: 1,
					width: 160,
					height: 45,
					speed: 400
				});
			}
			$( "#carousel" ).rcarousel({
				visible: 5,
				step: 1,
				width: 160,
				height: 45,
				speed: 400
			});
		}
	});
})(jQuery);