

/**=========================================================
 * SERVICE : PAGES
 =========================================================*/
App.service('Parametres', function($http, $q, $log, Module) {



});

/**=========================================================
 * Parametres
 =========================================================*/
App.controller('ParametresIndexCtrl', ['$scope', '$http', '$state', '$log', '$rootScope', '$routeParams', 'toaster', 'Parametres', function($scope, $http, $state, $log, $rootScope, $routeParams, toaster, Parametres) {

    $scope.data = {

        Parametres : []

    };

    var loadParams = function () {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/parametres/index')

            .success(function(data) {

                $rootScope.loading = false;

                $scope.data = data;

            })
            .error(function() {

                $rootScope.loading = false;

            });

    }

    loadParams();




    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        $http.post(WH_ROOT + '/admin/parametres/index', $scope.data)
            .success(function(data) {

                toaster.pop('success', 'Paramètres modifiés');

                $rootScope.loading = false;

            })
            .error(function(data) {

                $rootScope.loading = false;

            });


    };


}]);

