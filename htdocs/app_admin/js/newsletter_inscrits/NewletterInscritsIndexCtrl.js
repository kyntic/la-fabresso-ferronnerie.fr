App.controller('NewletterInscritsIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', 'toaster', 'NewsletterInscrit', function($scope, $log, $rootScope, $http, $state, toaster, NewsletterInscrit) {

    'use strict';

    $scope.log = '';

    $scope.inscrits = [];

    $scope.init = function () {

        $scope.loadInscrits();

    }

    /**
     * Charge la liste des familles d'attribut
     */
    $scope.loadInscrits = function () {

        $rootScope.loading = true;

        NewsletterInscrit.findAll().then(function (response) {

            if (response.statut == 1) {

                $scope.inscrits = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    $scope.exporter = function () {

        window.open(NewsletterInscrit.parametres.urls.exporter, '_blank');

    };

    /**
     * Lancement de la fonction init
     */
    $scope.init();

}]);
