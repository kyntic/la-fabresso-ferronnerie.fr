
/**=========================================================
 * CONTROLLER GROUPE INDEX
 =========================================================*/
App.controller('TypepagesCtrl', ['$scope', function ($scope) {
    this.parametres = {

        urls: {
            index           :   '/admin/typepages/index/',
            view            :   '/admin/typepages/view/',
            edit            :   '/admin/typepages/edit/',
            delete          :   '/admin/typepages/delete/'
        }

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.findAll = function (id) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.index;

        if (angular.isDefined(id)) {
            url += id;
        }

        $http.get(url)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.getById = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.view + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.save = function (data) {

        var _this = this, deffered = $q.defer();

        url = this.parametres.urls.edit;

        if (angular.isDefined(data.Typepage.id)) {
            url += data.Typepage.id;
        }

        $http.post(url, data)
            .then(function(response) {

                deffered.resolve(response.data);

            }, function(x) {

                deffered.reject(x);
            }
        );

        return deffered.promise;

    };

    /**
     * Récupérer toutes les familles d'attribut
     */
    this.delete = function (id) {

        var _this = this, deffered = $q.defer();

        $http.get(this.parametres.urls.delete + id)
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };


}]);

App.controller('TypepagesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$modal', 'toaster','Typepage', '$state', function ($scope, $log, $rootScope, $http, $modal, toaster, Typepage, $state) {

    'use strict';

    $scope.log = '';

    $scope.typepages = [];

    $scope.init = function () {

        $scope.load();

    }


    $scope.load = function () {

        $rootScope.loading = true;

        Typepage.findAll().then(function (response) {

            if (response.statut == 1) {

                console.log(data);
                $scope.typepages = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Delete
     */
    $scope.delete = function (index) {

        if (confirm('Etes vous sûre de supprimer ?')) {

            $http
                .post(WH_ROOT + '/admin/typepages/delete/' + $scope.Typepages[index].Typepage.id)

                .then(function (response) {

                    $scope.Typepages.splice(index, 1);
                    toaster.pop('success', 'Opération réussie', 'Type de page supprimée');

                }

            );

        }
        ;

    }


    /**
     * Edition d'un group
     */
    $scope.edit = function (index) {

        var modalInstance = $modal.open({
            templateUrl: '/typepage-edit.html',
            controller: ModalInstanceCtrl,
            resolve: {
                data: function () {

                    if (!isNaN(index)) {
                        return $scope.Typepages[index];
                    } else {
                        return {};
                    }

                }
            }
        }).result.then(function () {

                $scope.load();

            });
    };

    // Please note that $modalInstance represents a modal window (instance) dependency.
    // It is not the same as the $modal service used above.

    var ModalInstanceCtrl = function ($scope, $modalInstance, data) {

        $scope.data = data;


        $scope.ok = function () {

            $http
                .post(WH_ROOT + '/admin/typepages/edit', $scope.data)

                .then(function (response) {

                    $modalInstance.close();

                }, function (x) {

                    $scope.log = x;

                }

            );


        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };


}]);
