
/**=========================================================
 * CONTROLLER USERS INDEX
 =========================================================*/
App.controller('DashboardCtrl', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {


    $rootScope.Loading = true;

    $http.get(WH_ROOT + '/admin/homes/dashboard')
        .success(function(data) {

            $scope.data = data;



            $rootScope.Loading = false;
        });



}]);
