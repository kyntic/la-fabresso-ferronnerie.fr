

/**=========================================================
 * SERVICE : PAGES
 =========================================================*/
App.service('Actualite', function($http, $q, $log, Module) {

    this.params = {

        Url : {
            save    : WH_ROOT + '/admin/blog/actualites/edit',
            view    : WH_ROOT + '/admin/blog/actualites/view',
            all     : WH_ROOT + '/admin/blog/actualites/index/actu',
            delete  : WH_ROOT + '/admin/blog/actualites/delete/'
        }
    };

    this.Rubriques = [];

    this.Types = {
        actu        : 'Actualite',
        reference   : 'Référence'
    };

    /**
     * Récuéper toutes les actualités
     */
    this.getAll = function() {

    };

    /**
     * Récupérer
     * @param id
     */
    this.getById = function (id) {

        var _this = this;

        var d = $q.defer();

        var res = {
            Actualite : {
                etat_id                 : 'publish',
                publication_date_js     : new Date()
            },
            Contents : []
        };

        res.Contents.push(new Module.getInstance('txt'));

        $http

            .get(this.params.Url.view + '/' + id)

            .success(function(response) {

                if(angular.isObject(response.Parents)) _this.Rubriques = response.Parents;

                if(angular.isObject(response.Data.Actualite)) {

                    response.Data.Page.ids = $.map(response.Data.Page.ids, function(value, index) {
                        return [value];
                    });

                    angular.extend(res, response.Data);
                }

                d.resolve(res);

            }).error(function(res) {

                d.reject(res);
            }
        );

        return d.promise;
    };

    /**
     * Sauvegarder une actualité
     * @param data
     */
    this.save = function (data) {

        var _this = this;

        var d = $q.defer(); //Initialisation de la promesse de fonction

        console.log(data);
        $http
            .post(this.params.Url.save, data)

            .then(function(response) {

                d.resolve(response.data);

            }, function(x) {

                d.reject(x);
            }
        );

        return d.promise; //On retourne le résultat de promise
    };

    /**
     * Sauvegarder une actualité
     * @param data
     */
    this.delete = function (id) {

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http.get(this.params.Url.delete + id)

            .success(function(data) {

                if(data.ok) {

                    d.resolve('ok');

                }else{

                    d.reject('Une erreur est survenue ');

                }

            })
            .error(function(x) {

                d.reject(x);

            });

        return d.promise; //On retourne le résultat de promise
    };
});

/**=========================================================
 * ACTU : EDITION
 =========================================================*/
App.controller('ActualiteEditionCtrl', ['$scope', '$http', '$state', '$log', '$rootScope', '$routeParams', 'toaster', 'Actualite', 'Page', function($scope, $http, $state, $log, $rootScope, $routeParams, toaster, Actualite, Page) {

    /**
     * Calendrier
     */
    $scope.today        = function() {$scope.dt = new Date();};
    $scope.clear        = function () {$scope.dt = null;};
    $scope.open         = function($event) {$event.preventDefault();$event.stopPropagation();$scope.opened = true;};
    $scope.dateOptions  = {formatYear: 'yy', startingDay: 1};


    $scope.Types = Actualite.Types;

    /**
     * Données initiales
     *
     */


    /**
     * Rubriqes
     */
    $scope.Rubriques = [];

    Page.getList().then(
        function(data) {
            $scope.Rubriques = data;

        }
    );

    $scope.data         = [];
    $rootScope.loading  = true;

    Actualite.getById($state.params.id).then(function (data) {

        $scope.data = data;

        if($state.params.parent_id) $scope.data.Actualite.parent_id = $state.params.parent_id;

        $rootScope.loading = false;

    }, function() {

        toaster.pop('warning', 'Attention', 'Impossible de retrouver la page');

        $rootScope.loading = false;
    });

    /**
     * Enregistrement
     */
    $scope.submit = function() {

        $scope.log          = '';
        $rootScope.loading  = true;

        Actualite.save($scope.data).then(function(reponse) {

            $rootScope.loading = false;

            if (reponse.ok) {

                $scope.data.Actualite.id = reponse.id;

                toaster.pop('success', 'Enregistrement', 'Enregistrement effectué');

            } else {

                $scope.log = reponse;

                toaster.pop('warning', 'Attention', 'Une erreur est survenue');
            }
        }, function(erreur) {

            toaster.pop('danger', 'Erreur', erreur);

            $scope.log = erreur.data;

            $rootScope.loading = false;
        });
    };
}]);

App.controller('ActualitesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', 'Actualite', 'toaster', function($scope, $log, $rootScope, $http, $state, Actualite, toaster) {

    'use strict';

    /**
     * Liste des fichiers
     * @type {Array}
     */
    $scope.Actualite = [];

    $scope.Types = Actualite.Types;

    $scope.loadActualites = function () {

        $rootScope.loading = true;

        $http.get(WH_ROOT + '/admin/blog/actualites/index/').

            success(function(data) {

                $scope.Actualites = data;

                $rootScope.loading = false;

            }).
            error(function() {

                $rootScope.loading = false;

            });


    }

    $scope.loadActualites();



    /**
     * Delete
     */
    $scope.delete = function(index) {

        if(confirm('Etes vous sûre de vouloir supprimer cette page ? ')) {


            $rootScope.loading  = true;

            Actualite.delete($scope.Actualites[index].Actualite.id).then(function(reponse) {

                $rootScope.loading = false;

                toaster.pop('success', 'Actualité supprimée', '');

                $scope.Actualites.splice(index, 1);

            }, function(erreur) {

                console.log(erreur);

                toaster.pop('danger', 'Erreur', erreur);

                $rootScope.loading = false;
            });


        }

    };


}]);
