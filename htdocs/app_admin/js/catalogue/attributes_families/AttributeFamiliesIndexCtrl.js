App.controller('AttributeFamiliesIndexCtrl', ['$scope' , '$log', '$rootScope', '$http', '$state', 'toaster', 'AttributeFamily', function($scope, $log, $rootScope, $http, $state, toaster, AttributeFamily) {

    'use strict';

    $scope.log = '';

    $scope.attributeFamily = [];
    $scope.arborescence = [];

    $scope.init = function () {

        $scope.loadAttributeFamilies();
        $scope.loadArborescence();

    }

    /**
     * Charge la liste des familles d'attribut
     */
    $scope.loadAttributeFamilies = function () {

        $rootScope.loading = true;

        AttributeFamily.findAll($state.params.attribute_family_id).then(function (response) {

            if (response.statut == 1) {

                $scope.attributeFamily = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Charge la liste des familles d'attribut
     */
    $scope.loadArborescence = function () {

        $rootScope.loading = true;

        AttributeFamily.findTree().then(function (response) {

            if (response.statut == 1) {

                $scope.arborescence = response.data;

            } else {

                $scope.log = response;

            }

            $rootScope.loading = false;

        }, function (error) {

            $scope.log = error;

            $rootScope.loading = false;

        });

    }

    /**
     * Supression
     */
    $scope.delete = function(index) {

        if(confirm('Êtes-vous sûr de vouloir supprimer cette famille d\'attribut ? ')) {

            $rootScope.loading  = true;

            AttributeFamily.delete($scope.attributeFamily.children[index].AttributeFamily.id).then(function(response) {

                if (response.statut == 1) {

                    toaster.pop('success', 'Famille d\'attribut supprimée');

                    $scope.attributeFamily.children.splice(index, 1);

                } else {

                    $scope.log = response;

                }

                $rootScope.loading = false;

            }, function (error) {

                $scope.log = error;

                $rootScope.loading = false;

            });

        }

    };

    /**
     * Ordre
     */
    $scope.sortable = {

        stop: function (e, ui) {

            var attributeFamilies = $scope.attributeFamily.children.map(function (i) {
                return i.AttributeFamily.id;
            }).join(',');

            AttributeFamily.ordre(attributeFamilies).then(
                function (data) {

                    toaster.pop('success', 'Ordre enregistré');

                },
                function (data) {

                    $scope.log = data;

                    toaster.pop('danger', 'Une erreur est survenue');
                }
            );

        }

    };

    /**
     * Lancement de la fonction init
     */
    $scope.init();

}]);
