App.service('Page', function ($http, $q, $log, Module) {

    /**
     * The params
     *
     * @type {{Url: {save: string, all: string, liste: string, ordre: string, delete: string}}}
     */
    this.params = {

        Url: {
            save: WH_ROOT + '/admin/pages/edit',
            all: WH_ROOT + '/admin/pages/index',
            liste: WH_ROOT + '/admin/pages/liste',
            ordre: WH_ROOT + '/admin/pages/ordre',
            delete: WH_ROOT + '/admin/pages/delete/'
        }
    };

    /**
     * Templates
     *
     * @type {{page: string, home: string, faq: string, contact: string, actualites: string, liste_rss: string, evenements: string}}
     */
    this.Templates = {
    };

    // this.Templates = Typepage.getList();

    this.locale = 'fre';

    this.Menus = {
        0: 'Ne pas afficher dans le menu',
        1: 'Menu principal'

    };

    this.liste = [];
    this.loaded = false;

    /**
     * Récuéper toutes les pages
     */
    this.getAll = function () {

        return this.liste;
    };

    this.getRubriquesOptions = function () {

        var _this = this, deffered = $q.defer();

        $http.get(WH_ROOT + '/admin/pages/getRubriquesOptions/')
            .success(function (response) {

                deffered.resolve(response);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;

    };

    /**
     * Récuéper un tableau à deux dimenssions :
     */
    this.getList = function () {

        var _this = this, deffered = $q.defer();

        /**
         * Get the list from server
         */
        $http.get(this.params.Url.liste + '/')
            .success(function (response) {

                _this.liste = response;
                _this.loaded = true;

                deffered.resolve(_this.liste);
            })
            .error(function (x) {

                deffered.reject(x);
            });

        return deffered.promise;
    };

    /**
     * Récupérer
     * @param id
     */
    this.getById = function (id) {

        var _this = this;

        var res = {
            Page: {
                etat_id: 'publish',
                publication_date_js: new Date(),
                locale: 'fre',
                template: 'page'

            },
            Contents: []
        };

        res.Contents.push(new Module.getInstance('txt'));

        var d = $q.defer();

        $http
            .get(this.params.Url.save + '/' + id + '/' + _this.locale)

            .success(function (response) {

                if (angular.isObject(response.Parents)) _this.liste = response.Parents;

                if (angular.isObject(response.Data.Page)) angular.extend(res, response.Data);

                res.Page.locale = 'fre';

                d.resolve(res);

            }).error(function (res) {

                d.reject(res);
            }
        );

        return d.promise;
    };

    /**
     * Sauvegarder une actualité
     * @param data
     */
    this.save = function (data) {

        var _this = this;

        var deferred = $q.defer(); //Initialisation de la promesse de fonction

        $http
            .post(this.params.Url.save, data)

            .then(function (response) {

                //On met à jour la list des pages
                _this.loaded = false;

                deferred.resolve(response.data);

            }, function (x) {

                deferred.reject(x);
            }
        );

        return deferred.promise; //On retourne le résultat de promise
    };

    this.ordre = function (nouvelOrdre) {

        var d = $q.defer();

        $http
            .post(this.params.Url.ordre, {data: nouvelOrdre})

            .success(function (data) {
                d.resolve(data);
            })
            .error(function (data) {
                d.reject(data);
            });

        return d.promise;
    };

    /**
     * Sauvegarder une actualité
     * @param data
     */
    this.delete = function (id) {

        var d = $q.defer(); //Initialisation de la promesse de fonction

        $http.get(this.params.Url.delete + id)

            .success(function (data) {

                if (data.ok) {

                    d.resolve('ok');

                } else {

                    d.reject('Cette page contient des elle même des pages. Vous devez d’abord supprimer les pages enfants avant de supprimer celle-ci. ');
                }
            })
            .error(function (x) {

                d.reject(x);
            });

        return d.promise; //On retourne le résultat de promise d
    };
});