/**
 * Ldap Service | Get all the groups/users
 */
App.service('LdapGroup', function ($http, $q) {

    this.load       = false;
    this.ldapData   = [];

    /**
     * Get all the users/groups in LDAP
     */
    this.getAll = function () {
        var d       = $q.defer();
        var _this   = this;

        /**
         * If we have data
         */
        if (_this.ldapData.length > 0) {

            d.resolve(_this.ldapData);

            return d.promise;
        }

        $http.get(WH_ROOT + '/admin/ldap/ldap_groups/get_all')
            .success(function (data) {

                /**
                 * Assign the data
                 */
                _this.ldapData = data;

                d.resolve(_this.ldapData);
            })
            .error(function (e) {

                /**
                 * Unhandled error | Return message
                 */
                d.reject(e);
            });

        return d.promise;
    };

    this.getList = function() {

        this.getAll().then(

            function(data) {

                return data;

            }, function() {

                return [];
            }
        );
    };
});