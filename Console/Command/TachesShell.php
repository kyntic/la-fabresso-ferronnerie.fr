<?php
class TachesShell extends AppShell {
    

	var $uses = array(
		'Log', 
		'Tache' 
	);


    public function main() {
        $this->out('Hello world.');

    }

    public function bdo() {

    	$tache = $this->Tache->findById($this->args[0]);

    	//Etat commencé
		$this->Tache->save(array('id' => $this->args[0], 'etat' => 1));

    	//Lancement de la tache
		exec(APP.'cakephp/vendors/cakeshell '.$tache['Tache']['controller'].' '.$tache['Tache']['action'].' '.$tache['Tache']['donnees'].' -cli /usr/bin -console /cakephp/lib/Cake/Console -app ' . APP . ' ', $res);	

		$resultat = '';

		foreach($res as $v) $resultat .= $v."\r";		

		$this->Tache->save(array('id' => $this->args[0], 'etat' => 2, 'resultat' => serialize($res)));

		if(Configure::read('debug') == 2) {

			$email[] = Configure::read('Projet.email');

		}

        echo $resultat;

		App::uses('CakeEmail', 'Network/Email');

		$email = new CakeEmail();
		$email->config('smtp');
		$email->to($email);
		$email->from(Configure::read('Projet.email'));
		$email->subject('Résultat de la tâche de fond : '.Configure::read('Projet.nom'). ' - '.$tache['Tache']['controller'].' '.$tache['Tache']['action']);
		$email->emailFormat('text');
		$email->template('default');
		$email->viewVars();

		if($email->send($resultat)) {


		}else{

			echo 'erreur d’envoi de rapport de tâche. ';

		}


    }


    public function lancer() {

		exec(APP.'cakephp/vendors/cakeshell '.implode(' ', $this->args).' -cli /usr/bin -console /cakephp/lib/Cake/Console -app ' . APP . ' ', $res);	

		$resultat = '';

		foreach($res as $v) $resultat .= $v."\r";		

		if(Configure::read('debug') == 2) {

	        echo $resultat;

			App::uses('CakeEmail', 'Network/Email');

			$email = new CakeEmail();
			$email->config('smtp');
			$email->to(Configure::read('Projet.email_debug'));
			$email->from(Configure::read('Projet.email'));
			$email->subject('Résultat de la tâche de fond : '.Configure::read('Projet.nom'). ' - '.implode('/', $this->args));
			$email->emailFormat('text');
			$email->template('default');
			$email->viewVars();

			if($email->send($resultat)) {


			}else{

				echo 'erreur d’envoi de rapport de tâche. ';

			}


    	}

    }
}