<?php
/**
 * PERMET DE CREER TOUTES LES URLS, LES ENREGISTRER DANS LE MODEL
 * ET ECIRE LE HTACCESS
 *
 * Class MenuTreeShell
 */
class MenuTreeShell extends AppShell {
    
	var $uses = array(
        'ContentType'
	);

    public function main() {

        $this->out('Hello world.');

    }


    /**
     *
     * @param $id
     */
    public function fabrique_url () {

        $ContentType = array();

        $lst = $this->ContentType->find('all');

        foreach($lst as $v) $ContentType[$v['ContentType']['id']] = $v;

        $Model = ClassRegistry::init($this->args[1]);

        //Trouver tous les enfants :
        $enfants = $Model->children($this->args[0], false);

        //On ajoute l'id en cours
        $enfants[] = $Model->findById($this->args[0]);

        foreach($enfants as $v) {

            if(!empty($v[$this->args[1]]['content_type_id'])) $v = $v + $ContentType[$v[$this->args[1]]['content_type_id']];

            $url = '/'.$this->_url($this->args[1], $v);

            $Model->id = $v[$this->args[1]]['id'];
            $Model->saveField('url', $url);

        }

    }


    private function _url ($Model, $tab) {

        $Class = ClassRegistry::init($Model);

        $arbos = $Class->getPath($tab[$Model]['id']);

        $nouv_url = array();

        $masque = '#([0-9]*)#'; //Interdire les chiffres dans l'url pour eviter les pb de route

        if($arbos) {

            foreach($arbos as $arbo) {

                if(!empty($arbo[$Model]['slug'])) $arbo[$Model]['name'] = $arbo[$Model]['slug'];

                if(preg_match($masque, $arbo[$Model]['name'])) $url = preg_replace($masque, '', $arbo[$Model]['name']);

                $nouv_url[] = strtolower(Inflector::slug($arbo[$Model]['name'], '-'));

            }

        }else{

            if(!empty($tab[$Model]['slug'])) $tab[$Model]['name'] = $tab[$Model]['slug'];

            if(preg_match($masque, $tab[$Model]['name'])) $url = preg_replace($masque, '', $tab[$Model]['name']);

            $nouv_url[] = strtolower(Inflector::slug($tab[$Model]['name'], '-'));

        }


        if(!empty($tab[$Model]['model_id'])) $tab[$Model]['id'] = $tab[$Model]['model_id'];

        if(!empty($tab['ContentType'])) {

            $nouv_url[] = $tab['ContentType']['prefixe_url'].$tab[$Model]['id'];

        }else{

            $nouv_url[] = $tab[$Model]['id'];

        }

        $nouv_url = implode('/', $nouv_url);

        return $nouv_url;


    }

}