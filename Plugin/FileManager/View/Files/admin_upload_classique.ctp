<?php 
$param_url = array(); foreach($this->request->params['named'] as $k => $v) $param_url[] = $k.':'.$v; $param_url = implode('/', $param_url); //Traformation du tableau en chaine de paramètres pour les urls 
echo $this->Form->create('File', array('url' => '/admin/file_manager/files/upload/'.$param_url, 'type' => 'file', 'id' => 'fileupload', 'class' => 'form-horizontal'));

echo '<input id="fileupload" type="file" name="files[]" multiple>';

echo $this->Form->submit();

echo $this->Form->end(); 
?>
