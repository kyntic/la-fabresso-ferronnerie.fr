<?php
$this->assign('titre', '<i class="icon-list"></i> Logs des campagnes');
$this->Html->addCrumb(__('Liste des campanges'), '/admin/newsletter_manager/newsletter_campaigns/');
$this->Html->addCrumb(__('Logs des campagnes'), $this->here);
?>
<div class="box">
    <div class="box-content">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Date</th>
                <th>Etât</th>
                <th>Auteur</th>
                <th>Résultat de la requête</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($taskList as $task): ?>
                <?php
                    $date_1 = strtotime($task['Tache']['created']);
                    $date_2 = strtotime($task['Tache']['updated']);
                    $diff   = $date_2 - $date_1;
                    $diff   = round($diff / 60);
                ?>
                <tr>
                    <td><?=$task['Tache']['id'];?></td>
                    <td><?=date('d/m/Y');?></td>
                    <td><?=$task['Tache']['etat_name'];?></td>
                    <td><?=$task['User']['nom_connexion'];?></td>
                    <td><?=(is_array($task['Tache']['resultat'])) ? implode('<br />', $task['Tache']['resultat']) : $task['Tache']['resultat'];?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>