<?php
$this->assign('titre', '<i class="fa fa-list"></i> Edition d’une liste');
$this->Html->addCrumb(__('Gestion des listes'), '/admin/newsletter_manager/newsletter_lists');
?>
<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-12">

        <?php
        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Informations'
            )
        );
        ?>
        <div class="widget-body">

            <?=$this->Form->create('List', array('class' => 'form-inline'));?>

            <fieldset>
                <div class="form-group">
                    <?=$this->Form->input('NewsletterList.name', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Nom de la liste'));?>
                </div>
                <button type="submit" class="btn btn-primary">
                    Enregistrer
                </button>
            </fieldset>

            <?=$this->Form->end();?>

        </div>
        <div class="widget-body no-padding">

                <table class="table table-striped table-normal">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Prénom</th>
                        <th>Nom</th>
                        <th style="width: 478px;">
                            <?=$this->Html->link('<i class="fa fa-edit "></i> Importer depuis inscrits', '/admin/newsletter_manager/newsletter_emails/import_from_emails/' . $this->data['NewsletterList']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs', 'id' => 'import-from-emails'))?>
                            <?=$this->Html->link('<i class="fa fa-edit "></i> Importer depuis CSV', '/admin/newsletter_manager/newsletter_lists/import_from_csv/' . $this->data['NewsletterList']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs'))?>
                            <?=$this->Html->link('<i class="fa fa-edit "></i> Ajout email unique', '/admin/newsletter_manager/newsletter_emails/import_from_unique/' . $this->data['NewsletterList']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs', 'data-toggle' => 'modal', 'data-target' => '#popup_ajx'))?>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="list-emails">
                    <?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>
                    var chargeListeEmail = function(){
                    $('#list-emails').load('/admin/newsletter_manager/newsletter_emails/index/<?php echo $this->data['NewsletterList']['id']; ?>');
                    };
                    chargeListeEmail();

                    $('#import-from-emails').click(function(){
                    $.ajax({
                    type 		: 'POST',
                    url 		: $(this).attr('href'),
                    complete	: function () {chargeListeEmail();}
                    });
                    return false;
                    });
                    <?php $this->Html->scriptEnd(); ?>
                    </tbody>
                </table>


        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>
</div>
