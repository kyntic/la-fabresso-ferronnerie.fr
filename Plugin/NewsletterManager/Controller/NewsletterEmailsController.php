<?php

/**
 * Class NewsletterEmailsController
 */
class NewsletterEmailsController extends NewsletterManagerAppController
{
    public $uses = array(
        'NewsletterManager.ListEmail',
        'NewsletterManager.NewsletterEmail'
    );

    /**
     * Default admin index
     */
    public function admin_index($listId = null)
    {

        if ($listId !== null) {

            $emails = $this->ListEmail->find(
                'all',
                array(
                    'conditions' => array(
                        'ListEmail.list_id' => $listId
                    )
                )
            );

            $this->set('emails', $emails);
        }
    }

    /**
     * Delete all emails for a special list
     */
    public function admin_delete_all($listId = null)
    {

        if($listId !== null) {
            $this->ListEmail->deleteAll(
                array(
                    'ListEmail.list_id'   => $listId
                )
            );
        }
    }

    /**
     * Delete a single email
     *
     * @param null $listId
     * @param null $emailId
     */
    public function admin_delete($listId = null, $emailId = null)
    {
        if ($listId !== null && $emailId !== null) {
            //On detruit le lien - Les emails restent
            $this->ListEmail->deleteAll(
                array(
                    'ListEmail.list_id'   => $listId,
                    'ListEmail.email_id'  => $emailId
                )
            );
        }
    }

    /**
     * Function used to import data from subscribed people
     */
    public function admin_import_from_emails($listId = null)
    {

        $this->loadModel('NewsletterManager.NewsletterInscrit');
        $this->loadModel('User');

        $emailData      = array();
        $listEmailData  = array();

        $suscribedUsers = $this->NewsletterInscrit->find('all');

        foreach ($suscribedUsers as $user) {

            $id = null;

            $userInfo = $this->User->find(
                'first',
                array(
                    'conditions' => array(
                        'User.email' => $user['NewsletterInscrit']['email']
                    )
                )
            );

            $emailData['NewsletterEmail'] = array(
                'email'         => $user['NewsletterInscrit']['email'],
                'first_name'    => (isset($userInfo['User']['prenom'])) ? $userInfo['User']['prenom'] : '',
                'last_name'     => (isset($userInfo['User']['nom'])) ? $userInfo['User']['nom'] : '',
                'user_id'       => (isset($userInfo['User']['id'])) ? $userInfo['User']['id'] : 9999
            );

            if ($this->NewsletterEmail->save($emailData)) {
                //Get the last inserted ID
                $id = $this->NewsletterEmail->getLastInsertID();

                $this->NewsletterEmail->id = null;

            } else {
                //On recupere notre email qui existe deja
                $email = $this->NewsletterEmail->find(
                    'first',
                    array(
                        'conditions' => array(
                            'NewsletterEmail.email' => $emailData['NewsletterEmail']['email']
                        )
                    )
                );

                if (!$email) {
                    continue;
                }

                $id = $email['NewsletterEmail']['id'];
            }

            $relationExists = $this->ListEmail->find(
                'count',
                array(
                    'conditions' => array(
                        'list_id'   => $listId,
                        'email_id'  => $id
                    )
                )
            );

            if (!$relationExists) {

                $listEmailData['ListEmail'] = array(
                    'list_id'   => $listId,
                    'email_id'  => $id
                );

                $this->ListEmail->save($listEmailData);
                $this->ListEmail->id = null;
            }
        }

        exit();
    }

    /**
     * Import an unique email
     *
     * @param null $listId
     */
    public function admin_import_from_unique($listId = null)
    {
        $this->layout = 'admin_popup';

        $status = array();

        if ($listId == null) {

            $status['status'] = 0;
            exit(json_encode($status));
        }

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            //Un email est cree
            if ($this->NewsletterEmail->save($data)) {

                $emailId = $this->NewsletterEmail->getLastInsertID();

            } else {
                $email = $this->NewsletterEmail->find(
                    'first',
                    array(
                        'conditions' => array(
                            'NewsletterEmail.email' => $data['NewsletterEmail']['email']
                        )
                    )
                );

                if (!$email) {
                    $status['status'] = 0;
                    exit(json_encode($status));
                }

                //Si il existe, on l'update
                $this->NewsletterEmail->id = $email['NewsletterEmail']['id'];
                $this->NewsletterEmail->save($data);

                $emailId = $email['NewsletterEmail']['id'];
            }

            $relationExists = $this->ListEmail->find(
                'count',
                array(
                    'conditions' => array(
                        'list_id'   => $listId,
                        'email_id'  => $emailId
                    )
                )
            );

            if (!$relationExists) {
                $dataToSave = array(
                    'ListEmail' => array(
                        'list_id'   => $listId,
                        'email_id'  => $emailId
                    )
                );

                if ($this->ListEmail->save($dataToSave)) {
                    $status['status'] = 1;
                    exit(json_encode($status));
                }
            }

            $status['status'] = 0;
            exit(json_encode($status));
        }

        $this->set('listId', $listId);
    }

    public function admin_tmp_import()
    {
        $this->loadModel('NewsletterManager.NewsletterListEmail');

        $emails = $this->NewsletterEmail->find('all');

        foreach($emails as $k => $v)
        {
            $listEmail = array();
            $listeEmail['NewsletterListEmail']['id'] = null;
            $listeEmail['NewsletterListEmail']['list_id'] = $v['NewsletterEmail']['liste_id'];
            $listeEmail['NewsletterListEmail']['email_id'] = $v['NewsletterEmail']['id'];


            $this->NewsletterListEmail->save($listeEmail);
        }

        exit('Fin de traitement');
    }
}
