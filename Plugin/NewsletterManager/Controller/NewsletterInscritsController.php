<?php

/**
 * Class NewsletterInscritsController
 */
class NewsletterInscritsController extends NewsletterManagerAppController
{
    public function inscription()
    {
        $this->layout = 'ajax';

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            if ($this->NewsletterInscrit->find('first', array('conditions' => array('NewsletterInscrit.email' => $data['NewsletterInscrit']['email'])))) {

                if ($this->NewsletterInscrit->deleteAll(array('NewsletterInscrit.email' => $data['NewsletterInscrit']['email']))) {

                    $this->Session->setFlash('Ce mail a été désinscrit !', 'success');

                }

            } else {

                $this->NewsletterInscrit->set($data);

                if ($this->NewsletterInscrit->save($data)) {

                    $this->Session->setFlash('Votre mail a été ajouté !', 'success');

                } else {

                    $this->Session->setFlash('Veuillez saisir un email valide !', 'error');

                }

            }

            $this->redirect(Controller::referer());

        }

    }

    public function admin_index()
    {

        $inscrits = $this->NewsletterInscrit->find(
            'all'
        );

        exit(
            json_encode(
                array(
                    'statut'    =>  1,
                    'data'      =>  $inscrits
                )
            )
        );

    }

    public function admin_exporter()
    {

        $inscrits = $this->NewsletterInscrit->find(
            'all'
        );
        $this->set('inscrits', $inscrits);

        $this->layout = 'export_xls';

    }

}
