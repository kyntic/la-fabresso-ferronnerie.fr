<?php

/**
 * Class NewsletterCampainsController
 */
class NewsletterCampaignsController extends NewsletterManagerAppController
{
    /**
     * Default admin index
     */
    public function admin_index()
    {
        $this->loadModel('NewsletterManager.NewsletterList');

        $campaigns = $this->NewsletterCampaign->find('all', array('order' => 'NewsletterCampaign.updated DESC'));

        echo json_encode($campaigns);

        exit();
    }

    /**
     * Get messages by type
     *
     * @param null $type
     */
    public function admin_get_by_type($type = null)
    {
        /**
         * No type defined
         */
        if ($type == null) {

            exit(json_encode(array()));
        }

        $return = array();

        switch ($type) {

            case 'sms' :

                $return = $this->NewsletterCampaign->find(
                    'all',
                    array(
                        'conditions' => array(
                            'NewsletterCampaign.type'   => 'sms',
                            'NewsletterCampaign.draft'  => false
                        )
                    )
                );

                break;

            case 'email' :

                $return = $this->NewsletterCampaign->find(
                    'all',
                    array(
                        'conditions' => array(
                            'NewsletterCampaign.type'   => 'email',
                            'NewsletterCampaign.draft'  => false
                        )
                    )
                );

                break;

            case 'draft' :

                $return = $this->NewsletterCampaign->find(
                    'all',
                    array(
                        'conditions' => array(
                            'NewsletterCampaign.draft'  => true
                        )
                    )
                );

                break;
        }

        exit(json_encode($return));
    }

    /**
     * Save draft
     */
    public function admin_save_draft()
    {
        if (!empty($this->request->data)) {

            $data = $this->request->data;

            /**
             * Assign the message to the right place
             */
            if ($data['NewsletterCampaign']['type'] == 'sms') {

                $data['NewsletterCampaign']['message'] = $data['NewsletterCampaign']['sms_message'];

            } else {

                $data['NewsletterCampaign']['message'] = $data['NewsletterCampaign']['mail_message'];
            }

            if ($this->NewsletterCampaign->save($data)) {

                $response = array(
                    'ok'        => true,
                    'message'   => 'Brouillon sauvegardé',
                    'id'        => $this->NewsletterCampaign->id
                );

                exit(json_encode($response));
            }

            $response = array(
                'ok'        => false,
                'message'   => 'Brouillon non sauvegardé',
                //'id'        => $data['NewsletterCampaign']['id']
            );

            exit(json_encode($response));
        }
    }

    /**
     * Edit a campaign and send it
     */
    public function admin_edit()
    {
        /**
         * The model we need
         */
        $this->loadModel('NewsletterManager.NewsletterList');

        /**
         * If we have data
         */
        if (!empty($this->request->data)) {

            $data = $this->request->data;

            /**
             * Assign the message to the right place
             */
            if ($data['NewsletterCampaign']['type'] == 'sms') {

                $data['NewsletterCampaign']['message'] = $data['NewsletterCampaign']['sms_message'];

            } else {

                $data['NewsletterCampaign']['message'] = $data['NewsletterCampaign']['mail_message'];
            }

            $data['NewsletterCampaign']['draft'] = false;

            if ($this->NewsletterCampaign->save($data)) {

                /**
                 * The campaign ID
                 */
                $campaignId = $this->NewsletterCampaign->id;

                switch ($data['NewsletterCampaign']['type']) {

                    case 'sms' :

                        if (isset($data['NewsletterCampaign']['envoi_test']) &&
                            !empty($data['NewsletterCampaign']['phone_number_test'])
                        ) {
                            $this->_testSms($campaignId, $data['NewsletterCampaign']['phone_number_test']);

                            $response['ok']         = true;
                            $response['message']    = 'SMS envoyé';
                            $response['id']         = $campaignId;

                            exit(json_encode($response));
                        }

                        if ($data['NewsletterCampaign']['etat_id'] == 'a_envoyer') {

                            if (!$this->_launchSmsCampaign($campaignId, true)) {

                                $response['ok']         = false;
                                $response['message']    = 'Messages non envoyés';
                                $response['id']         = $campaignId;

                                exit(json_encode($response));
                            }

                            $response['ok']         = true;
                            $response['message']    = 'Messages en cours d\'envoi';
                            $response['id']         = $campaignId;

                            exit(json_encode($response));
                        }

                        break;

                    case 'email' :

                        if (isset($data['NewsletterCampaign']['envoi_test']) &&
                            !empty($data['NewsletterCampaign']['email_test'])
                        ) {
                            $this->_testEmail($campaignId, $data['NewsletterCampaign']['email_test']);

                            $response['ok']         = true;
                            $response['message']    = 'Message envoyé';
                            $response['id']         = $campaignId;

                            exit(json_encode($response));

                        }

                        if ($data['NewsletterCampaign']['etat_id'] == 'a_envoyer') {

                            if (!$this->_launchEmailCampaign($campaignId, true)) {

                                $response['ok']         = false;
                                $response['message']    = 'Messages non envoyés';
                                $response['id']         = $campaignId;

                                exit(json_encode($response));
                            }

                            $response['ok']         = true;
                            $response['message']    = 'Messages en cours d\'envoi';
                            $response['id']         = $campaignId;

                            exit(json_encode($response));
                        }

                        break;

                    default :

                        /**
                         * We have no type
                         */
                        $response['ok']         = false;
                        $response['message']    = 'Message non envoyé';
                        $response['id']         = $campaignId;

                        exit(json_encode($response));

                        break;
                }
            } else {

                $response['ok']     = false;
                $response['error']  = array(
                    'NewsletterCampaign' => $this->NewsletterCampaign->invalidFields()
                );

                exit(json_encode($response));
            }
        }

        exit();
    }

    /**
     * Voir un message + param par default
     *
     * @param null $id
     */
    public function admin_view($id = null)
    {
        /**
         * The models we need
         */
        $this->loadModel('NewsletterManager.NewsletterList');
        $this->loadModel('Parametre');

        $parameter = $this->Parametre->find('first');

        /**
         * Default values
         */
        $response['data'] = array(
            'NewsletterCampaign' => array(
                'type'                      => 'email',
                'envoi_test'                => true,
                'default_from_name'         => $parameter['Parametre']['email_name'],
                'default_from_email'        => $parameter['Parametre']['email_noreply'],
                'default_from_telephone'    => $parameter['Parametre']['telephone_noreply'],
                'send_to'                   => array(),
                'list_id'                   => $this->NewsletterList->field(
                    'id',
                    array(
                        'default' => 1
                    )
                ),
            )
        );

        if (!empty($id)) {

            $response['data'] = $this->NewsletterCampaign->findById($id);
        }

        exit(json_encode($response));
    }

    /**
     * Send the job for campaign
     *
     * @param   null    $campaignId
     * @param   bool    $execute
     *
     * @return  bool
     */
    private function _launchEmailCampaign($campaignId = null, $execute = false)
    {
        /**
         * The model we need
         */
        $this->loadModel('Tache');

        if ($execute && $campaignId !== null) {

            if ($this->Tache->saveAll(
                array(
                    'Tache' => array(
                        'controller' => 'campaigns',
                        'action'     => 'sendcampaign',
                        'author'     => 'admin',
                        'donnees'    => $campaignId,
                        'user_id'    => AuthComponent::user('id'),
                        'fond'       => 1
                    )
                )
            )) {
                return true;
            }
        }

        return false;
    }

    /**
     * Launch SMS Campaign
     *
     * @param   null    $campaignId
     * @param   bool    $execute
     * @return  bool
     */
    private function _launchSmsCampaign($campaignId = null, $execute = false)
    {
        /**
         * The model we need
         */
        $this->loadModel('Tache');

        if ($execute && $campaignId !== null) {

            if ($this->Tache->saveAll(
                array(
                    'Tache' => array(
                        'controller' => 'campaigns',
                        'action'     => 'sendsmscampaign',
                        'author'     => 'admin',
                        'donnees'    => $campaignId,
                        'user_id'    => AuthComponent::user('id'),
                        'fond'       => 1
                    )
                )
            )) {
                return true;
            }
        }

        return false;
    }

    /**
     * Function to launch a test Email
     *
     * @param   int     $id
     * @param   string  $testEmail
     * @return  bool
     */
    private function _testEmail($id, $testEmail)
    {
        App::uses('CakeEmail', 'Network/Email');

        $email      = new CakeEmail();
        $campaign   = $this->NewsletterCampaign->findById($id);

        $email->from(array($campaign['NewsletterCampaign']['default_from_email'] => $campaign['NewsletterCampaign']['default_from_name']));
        $email->to($testEmail);
        $email->emailFormat('html');
        $email->subject($campaign['NewsletterCampaign']['default_from_subject']);

        if ($email->send($campaign['NewsletterCampaign']['message'])) {

            return true;
        }
    }

    /**
     * Test SMS
     *
     * @param null $campaignId
     * @param null $testPhoneNumber
     */
    private function _testSms($campaignId = null, $testPhoneNumber = null)
    {
        /**
         * No test phone number
         */
        if ($testPhoneNumber == null || $campaignId == null) {

            exit(
                json_encode(
                    array(
                        'ok'        => false,
                        'message'   => 'Paramètres manquants'
                    )
                )
            );
        }

        /**
         * Load primotexto
         */
        App::import('Vendor', 'primotexto/BaseManager');

        /**
         * Load the campaign
         */
        $campaign = $this->NewsletterCampaign->findById($campaignId);

        if (!$campaign) {

            exit(
                json_encode(
                    array(
                        'ok'        => false,
                        'message'   => 'Campagne non trouvée'
                    )
                )
            );
        }

        /**
         * Login
         */
        CredentialsHelper::setCredentials('jordi@whatson-web.com', 'xhb3hcbo1501');

        /**
         * Sends SMS
         */
        CampaignsManager::sendSms(
            str_replace(' ', '', $testPhoneNumber),
            $campaign['NewsletterCampaign']['message'],
            'Marcq',
            'sms_particulier',
            'TestApi'
        );

        exit(json_encode(array('ok' => true)));
    }

    /**
     * Admin delete
     *
     * @param $id
     */
    public function admin_delete($id)
    {
        $response['ok'] = false;

        $camp = $this->NewsletterCampaign->findById($id);

        if ($camp['NewsletterCampaign']['send']) {

            $response['message'] = 'Message envoyé';

        } else {

            $this->NewsletterCampaign->delete($id);

            $response['ok'] = true;
        }

        echo json_encode($response);

        exit();
    }
}