<?php

/**
 * Class NewsletterTemplatesController
 */
class NewsletterTemplatesController extends NewsletterManagerAppController
{

    /**
     * Default index
     */
    public function admin_index()
    {
        $templates = $this->NewsletterTemplate->find('all');

        $this->set('templates', $templates);
        $this->set('MenuAdminActives', array(80, 83));
    }

    /**
     * Add a template
     */
    public function admin_add()
    {
        if(!empty($this->request->data)){

            $data = $this->request->data;

            $uniqueId = uniqid();

            $data['NewsletterTemplate']['folder_name'] = $uniqueId;

            if ($this->NewsletterTemplate->save($data)) {

                $this->Session->setFlash('Template créé', 'admin_success');

                $id = $this->NewsletterTemplate->getLastInsertID();

                if ($id) {

                    $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $id);
                }
            }
        }

        $this->layout = 'admin_popup';
    }

    /**
     * Edit a template
     *
     * @param null $id
     */
    public function admin_edit($id = null)
    {
        if ($id == null) {
            $this->redirect('/admin/newsletter_manager/newsletter_campaigns/');
        }

        if (!empty($this->request->data)) {

            $data = $this->request->data;

            $htmlFile = $this->_getFilename($data['NewsletterTemplate']['folder_name']);

            if (isset($data['NewsletterTemplate']['content'])) {
                $folderUpdated = $this->_replaceTemplateContent($data['NewsletterTemplate']['folder_name'], $htmlFile, $data['NewsletterTemplate']['content']);

                if ($folderUpdated) {
                    $this->Session->setFlash('Template updaté !', 'admin_success');
                } else {
                    $this->Session->setFlash('Template non updaté !', 'admin_error');
                }
            }
        }

        $template   = $this->NewsletterTemplate->findById($id);
        $this->data = $template;

        $templateContent = $this->_readTemplateContent($template['NewsletterTemplate']['folder_name']);

        $this->set('httpLink', $this->_buildHttpLink($template['NewsletterTemplate']['folder_name']));
        $this->set('templateFolder', $this->_templatesFolder);
        $this->set('templateContent', $templateContent);
        $this->set('MenuAdminActives', array(80, 83));
    }

    /**
     * Delete a template
     *
     * @param null $id
     */
    public function admin_delete($id = null)
    {

    }

    /**
     * Function used to upload templates
     */
    public function admin_upload_templates($templateId = null)
    {
        App::uses('Folder', 'Utility');

        if ($templateId == null) {
            $this->redirect('/admin/newsletter_manager/newsletter_templates/');
        }

        if (!isset($this->data['NewsletterTemplate']['file'])) {

            $this->Session->setFlash('Zip non uploadé !', 'admin_error');
            $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $templateId);
        }

        $fileInfo = $this->data['NewsletterTemplate']['file'];

        if ($fileInfo['type'] !== 'application/zip') {
            $this->Session->setFlash('Zip non uploadé ! Mauvais format de fichier !', 'admin_error');
            $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $templateId);
        }

        $templateInfo = $this->NewsletterTemplate->findById($templateId);

        if (!$templateInfo) {
            $this->Session->setFlash('Zip non uploadé ! Template id non trouvé !', 'admin_error');
            $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $templateId);
        }

        $baseFolder = new Folder($this->_templatesFolder);

        //On cree un folder uniqueid pour accueillir notre zip
        $baseFolder->create($this->_templatesFolder . $templateInfo['NewsletterTemplate']['folder_name']);

        if (!move_uploaded_file($fileInfo['tmp_name'], $this->_templatesFolder . $templateInfo['NewsletterTemplate']['folder_name'] . '/' . $fileInfo['name'])) {
            $this->Session->setFlash('Zip non uploadé !', 'admin_error');
            $this->redirect('/admin/newsletter_manager/newsletter_campaigns/edit/' . $templateId);
        }

        //The zip file
        $zipFile = $this->_templatesFolder . $templateInfo['NewsletterTemplate']['folder_name'] . '/' . $fileInfo['name'];

        //Unzip the folder
        $zip = new ZipArchive();

        if ($zip->open($zipFile) === true) {
            for($i = 0; $i < $zip->numFiles; $i++) {
                $filename = $zip->getNameIndex($i);
                $fileInfo = pathinfo($filename);
                copy("zip://" . $zipFile . "#" . $filename, $this->_templatesFolder . $templateInfo['NewsletterTemplate']['folder_name'] . '/' . $fileInfo['basename']);
            }

            $zip->close();

            $this->NewsletterTemplate->id = $templateId;
            $this->NewsletterTemplate->set('is_uploaded', 1);
            $this->NewsletterTemplate->save();

            $this->Session->setFlash('Zip uploadé !', 'admin_success');
            $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $templateId);
        }

        $this->Session->setFlash('Zip non uploadé !', 'admin_error');
        $this->redirect('/admin/newsletter_manager/newsletter_templates/edit/' . $templateId);
    }

    /**
     * Read the content of the template
     *
     * @param null $folderName
     * @return bool
     */
    private function _readTemplateContent($folderName = null)
    {
        App::uses('File', 'Utility');

        if ($folderName == null) {
            return false;
        }
        //Find the html files
        $file = $this->_getFilename($folderName);

        if (!$file) {
            return false;
        }

        //On prend le premier .html par defaut
        $fileToEdit = new File($this->_templatesFolder . $folderName . '/' . $file, false);

        if ($fileToEdit) {
            return $fileToEdit->read();
        }

        return false;
    }

    /**
     * Function used to write the edited template
     *
     * @param $folderName
     * @param $fileName
     * @param $content
     *
     * @return bool
     */
    private function _replaceTemplateContent($folderName = null, $fileName = null, $content)
    {
        App::uses('File', 'Utility');

        if ($folderName == null || $fileName == null) {
            return false;
        }

        $file = new File($this->_templatesFolder . $folderName . '/' . $fileName, false);

        if ($file->write($content, 'w+')) {
            $file->close();
            return true;
        }

        $file->close();
        return false;
    }

    /**
     * Get the first html file
     *
     * @param null $folderName
     * @return bool
     */
    private function _getFilename($folderName = null)
    {
        App::uses('Folder', 'Utility');
        App::uses('File', 'Utility');

        if ($folderName == null) {
            return false;
        }

        $folder = new Folder($this->_templatesFolder . $folderName);
        $files  = $folder->find('[^.].*\.html');

        if (!$files) {
            return false;
        }

        unset($folder);

        //On retourne le premier fichier HTML par defaut
        return $files[0];
    }

    /**
     * Build http link
     *
     * @param $folderName
     * @return string
     */
    private function _buildHttpLink($folderName)
    {
        return FULL_BASE_URL . '/' . $this->_templatesFolder . $folderName . '/';
    }
}