<?php

/**
 * Class NewsletterEmail
 */
class NewsletterEmail extends AppModel
{

    /**
     * Data validation
     *
     * @var array
     */
    public $validate = array(
        'email' => array(
            'rule' => 'isUnique',
        )
    );
}