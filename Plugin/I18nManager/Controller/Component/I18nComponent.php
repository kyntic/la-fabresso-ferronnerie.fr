<?php

/**
 * Needed to be loaded from other classes
 */
App::uses('Component', 'Controller');

/**
 * Class UserComponent
 */
class I18nComponent extends Component
{


    public function majI18n ($plugin = '', $model = '', $champs = array(), $langues = array())
    {

        $I18nModel = ClassRegistry::init('I18nModel');

        $loadModel = $model;
        if (!empty($plugin)) {

            $loadModel = $plugin . '.' . $model;
        }
        $Model = ClassRegistry::init($loadModel);

        $i18nDatas = array();
        $tmpI18nDatas = $I18nModel->find(
            'all',
            array(
                'conditions' => array(
                    'I18nModel.model' => $model,
                    'I18nModel.field' => $champs
                )
            )
        );



        if (!empty($tmpI18nDatas)) {

            foreach ($tmpI18nDatas as $k => $v) {
                $i18nDatas[$v['I18nModel']['locale']][$v['I18nModel']['field']][$v['I18nModel']['foreign_key']] = $v['I18nModel']['id'];
            }

        }

        $contents = $Model->find(
            'all'
        );
        if (!$contents) {

            return false;
        }

        foreach ($langues as $langue) {

            foreach ($champs as $champ) {

                foreach ($contents as $content) {

                    if (!isset($i18nDatas[$langue][$champ][$content[$model]['id']])) {

                        $i18n = array();

                        $i18n['I18nModel']['id'] = null;
                        $i18n['I18nModel']['locale'] = $langue;
                        $i18n['I18nModel']['model'] = $model;
                        $i18n['I18nModel']['foreign_key'] = $content[$model]['id'];
                        $i18n['I18nModel']['field'] = $champ;
                        if (is_object($content[$model][$champ])) {

                            $content[$model][$champ] = json_encode($content[$model][$champ]);
                        }
                        $i18n['I18nModel']['content'] = $content[$model][$champ];

                        if (!$I18nModel->save($i18n)) {

                            return false;
                        }

                    }

                }

            }

        }

        return true;

    }


}