<div class="page-wrapper">
    <header class="page-heading clearfix">
        <h1 class="heading-title pull-left"><?= $Cont['Contact']['h1']; ?></h1>
        <div class="breadcrumbs pull-right">
            <ul class="breadcrumbs-list">
                <?= $this->Html->getCrumbsPublic('', array(
                    'text' => __('Accueil'),
                    'url' => '/',
                    'escape' => false,
                ));
                ?>
            </ul>
        </div><!--//breadcrumbs-->
    </header>

    <div class="page-content">
        <div class="row">
            <article class="contact-form col-md-8 col-sm-7 page-row">
                <h3 class="title">Lorem ipsum dolor si amet</h3>
                <p>We’d love to hear from you. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut erat magna. Aliquam porta sem a lacus imperdiet posuere. Integer semper eget ligula id eleifend. </p>
                <form>
                    <div class="form-group name">
                        <label>Nom et prénom</label>
                        <input type="text" class="form-control" placeholder="Entrer votre nom et prénom">
                    </div><!--//form-group-->
                    <div class="form-group email">
                        <label for="email">E-mail<span class="required">*</span></label>
                        <input id="email" type="email" class="form-control" placeholder="Entrer votre e-mail">
                    </div><!--//form-group-->
                    <div class="form-group email">
                        <label for="phone">Téléphone</label>
                        <input id="phone" type="tel" class="form-control" placeholder="Entrer votre numéro de téléphone">
                    </div><!--//form-group-->
                    <div class="form-group message">
                        <label for="message">Message<span class="required">*</span></label>
                        <textarea id="message" class="form-control" rows="6" placeholder="Entrer votre message ici ..."></textarea>
                    </div><!--//form-group-->
                    <button type="submit" class="btn btn-theme">Envoyer mon message</button>
                </form>
            </article><!--//contact-form-->
            <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">
                <section class="widget has-divider">
                    <h3 class="title">Notre vieux Collège</h3>
                    <p>Donec pulvinar arcu lacus, vel aliquam libero scelerisque a. Cras mi tellus, vulputate eu eleifend at, consectetur fringilla lacus. Nulla ut purus.</p>
                    <a class="btn btn-theme" href="#"><i class="fa fa-download"></i> Télécharger la brochure</a>
                </section><!--//widget-->

                <section class="widget has-divider">
                    <h3 class="title">Adresse postale</h3>
                    <p class="adr">
                        <span class="adr-group">
                            <span class="street-address">MARCQ INSTITUTION</span><br>
                            <span class="region">170 rue du Collège</span><br>
                            <span class="postal-code">59702 Marcq-en-Baroeul</span><br>
                        </span>
                    </p>
                </section><!--//widget-->

                <section class="widget">
                    <h3 class="title">Contact rapide</h3>
                    <p class="tel"><i class="fa fa-phone"></i>Tél: 03 20 65 92 30</p>
                    <p class="email"><i class="fa fa-envelope"></i>Email: <a href="#">contact@marcq-institution.com</a></p>
                </section>
            </aside><!--//page-sidebar-->
        </div><!--//page-row-->
        <div class="page-row">
            <article class="map-section">
                <h3 class="title">Où nous trouver ? </h3>
                <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2528.1083922188564!2d3.1055900000000025!3d50.680813000000015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c32bd5906b6acd%3A0x94c2ccf92f93975f!2sMarcq+Institution!5e0!3m2!1sfr!2sfr!4v1416491916228" width="100%" height="300" frameborder="0" style="border:0"></iframe></div><!--//map-->
            </article><!--//map-->
        </div><!--//page-row-->
    </div>
</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="/college-green/assets/plugins/gmaps/gmaps.js"></script>
<script type="text/javascript" src="/college-green/assets/js/map.js"></script>

