<?php 
$this->assign('titre', 'Créer une page contact');
echo $this->Smart->create('Contact', array('class' => 'smart-form'));
?>

<div class="modal-body">
    <fieldset>
        <?=$this->Smart->input('Contact.name', __('Nom'));?>
        <?=$this->Smart->select('MenuItem.parent_id', 'Page parente : ' , $MenuItems, array('col' => 8, 'empty' => 'Racine', 'class' => 'select2'));?>
    </fieldset>
</div>
<div class="modal-footer">
    <footer>

        <?php echo $this->Form->button(__('Enregistrer'), array('type' => 'submit', 'class' => 'btn btn-primary btn-xs'));?>
        <?php echo $this->Form->button(__('Enregistrer et editer'). ' <i class="fa fa-edit"></i>', array('type' => 'submit', 'class' => 'btn btn-success btn-xs', 'name' => 'data[Contact][edit]', 'value' => true));?>

    </footer>
</div>

<?=$this->Smart->end();?>
<script type="text/javascript" >
	

</script>

