<?php $this->Html->css('produit.css', null, array('inline' => false)); ?>

<?php if (!empty($product['Product']['resume']) && empty($product['Product']['meta_description'])) $product['Product']['meta_description'] = $product['Product']['resume']; ?>
<?php $this->element('balises_metas', array('metas' => $product['Product'])); ?>

<?php
    if (!empty($_breadcrumb['Chemin'])) {
        foreach ($_breadcrumb['Chemin'] as $v) {
            $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);
        }        
    }
?>

<?php $this->start('entete'); ?>

    <div id="entete" style="background-image:url('<?=isset($product['Rubrique']['img_entete']->File->url) ? $product['Rubrique']['img_entete']->File->url : ''; ?>');" >

        <div class="container">
            <h2 class="h1" ><span><?= $product['Rubrique']['h1']; ?></span></h2>
            <p><?= nl2br($product['Rubrique']['resume']); ?></p>
        </div>

    </div>

<?php $this->end(); ?>

<div class="row">

    <div class="col-md-3">

        <h2 class="h1"><span>Affiner la recherche</span></h2>
        <?= $this->element('Catalogue.filtres'); ?>

    </div>

    <div class="col-md-9">

        <div class="blocBlanc">

            <div class="row">

                <div class="col-md-8">

                    <?php  if (!empty($product['Product']['galerie']->files)) { ?>

                        <div id="carouselProduit" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner" role="listbox">

                                <?php foreach ($product['Product']['galerie']->files as $k => $v) { ?>

                                    <div class="item <?php if($k == 0) echo 'active' ?>">
                                        <?= $this->Html->link($this->WhFile->show($v, array('x' => 768, 'y' => 768), array('alt' => $product['Product']['name'])), $v->File->url, array('escape' => false, 'class' => 'fresco', 'data-fresco-group' => 'carouselProduit')); ?>
                                    </div>

                                <?php } ?>

                            </div>

                            <ol class="carousel-indicators">

                                <?php foreach ($product['Product']['galerie']->files as $k => $v) { ?><!--

                                 --><li data-target="#carouselProduit" data-slide-to="<?= $k; ?>" class="<?php if($k == 0) echo 'active' ?>">
                                        <?= $this->WhFile->show($v, array('x' => 768, 'y' => 768), array('class' => 'img-responsive')); ?>
                                    </li><!--

                             --><?php } ?>

                            </ol>

                        </div>

                        <?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

                            $('#carouselProduit').carousel(
                                {
                                    interval    :   5000
                                }
                            );

                        <?php $this->Html->scriptEnd(); ?>

                    <?php } ?>

                </div>

                <div class="col-md-4">

                    <h1><?= $product['Product']['h1']; ?></h1>
                    <p class="resume"><?= $product['Product']['resume']; ?></p>
                    <?= $product['Product']['description']; ?>

                    <div class="barre"></div>

                    <?php if (!empty($attributes)) { ?>

                        <section class="listeAttributes">

                            <div class="row">

                                <?php foreach ($attributes as $attribute) { ?>

                                    <div class="col-md-6">

                                        <div class="attribute">
                                            <?= $this->WhFile->show($attribute['Attribute']['vignette']); ?>
                                            <span><?= $attribute['Attribute']['name']; ?></span>
                                        </div>

                                    </div>

                                <?php } ?>
                                
                            </div>

                        </section>

                        <div class="barre"></div>

                    <?php } ?>

                    <?php if ($product['Product']['prix_barre_ttc'] > 0) { ?>

                        <p class="prix promotion"><?= $this->WhPrix->show($product['Product']['prix_ttc']); ?><span class="ttc">TTC/m&sup2;</span></p>
                        <p class="prixBarre"><?= $this->WhPrix->show($product['Product']['prix_barre_ttc']); ?><span class="ttc">TTC/m&sup2;</span></p>
                        <div class="barre"></div>
                        <div class="boutons">
                            <?= $this->Html->link('Demander un devis', '#', array('class' => 'demanderDevis promotion', 'data-href' => '/ecommerce/quotations/ajouterProduit/' . $product['Product']['id'])); ?><br>
                            <?= $this->Html->link('Découvrir en magasin', '/pages/view/14', array('class' => 'decouvrirMagasin')); ?><br>
                        </div>
                        <p class="reference">Ref : <?= $product['Product']['reference']; ?></p>

                    <?php } else { ?>

                        <p class="prix"><?= $this->WhPrix->show($product['Product']['prix_ttc']); ?><span class="ttc">TTC/m&sup2;</span></p>
                        <div class="barre"></div>
                        <div class="boutons">
                            <?= $this->Html->link('Demander un devis', '#', array('class' => 'demanderDevis', 'data-href' => '/ecommerce/quotations/ajouterProduit/' . $product['Product']['id'])); ?><br>
                            <?= $this->Html->link('Découvrir en magasin', '/pages/view/14', array('class' => 'decouvrirMagasin')); ?><br>
                        </div>
                        <p class="reference">Ref : <?= $product['Product']['reference']; ?></p>

                    <?php } ?>

                </div>

            </div>

            <?php $this->Html->scriptStart($options = array('block' => 'scriptBottom')); ?>

                $('.demanderDevis').click(function(){

                    window.location.href = $(this).data('href');
                    return false;

                });

            <?php $this->Html->scriptEnd(); ?>

        </div>

        <div class="blocGris">
            <?=$product['Product']['caracteristiques']; ?>
        </div>

    </div>

</div>

<?php if (!empty($autreProducts)) { ?>

    <h2 class="h1"><span>Autres décors disponibles</span></h2>

    <section class="listeAutreProducts">

        <div class="row">

            <?php foreach ($autreProducts as $autreProduct) { ?>

                <div class="col-md-6 col-sm-6">

                    <div class="produit">

                        <div class="row">

                            <div class="col-md-6">
                                <div class="image">
                                    <?= $this->WhFile->show($autreProduct['Product']['vignette'], array(), array('class' => 'img-responsive', 'url' => $autreProduct['Product']['url'], 'alt' => $autreProduct['Product']['name'])); ?>
                                    <div class="date">
                                        <?= $this->WhDate->show($autreProduct['Product']['created'], 'dateActualite'); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <h3><?= $this->Html->link('> ' . $autreProduct['Product']['name'], $autreProduct['Product']['url']); ?></h3>
                                <p><?= nl2br($autreProduct['Product']['resume']); ?></p>
                                <p class="lireSuite"><?= $this->Html->link('> Lire la suite...', $autreProduct['Product']['url']); ?></p>
                            </div>

                        </div>

                    </div>

                </div>

            <?php } ?>

        </div>

    </section>

<?php } ?>

<?php if (!empty($actualites)) : ?>
    <h2 class="h1"><span><?=!empty($product['Product']['realisation_title']) ? $product['Product']['realisation_title'] : 'Nos réalisations' ;?></span></h2>
    <?=$this->element('Blog.listeActualites'); ?>
<?php endif; ?>
