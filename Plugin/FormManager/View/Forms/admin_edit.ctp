<?php
$this->assign('titre', 'Edition d\'un formulaire');
$this->Html->addCrumb(__('Gestion des formulaires'), '/admin/form_manager/forms');
?>

<div class="row">
    <article class="col-sm-12 col-md-12 col-lg-4">

        <?php

        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Données du formulaire',
                'btns'  => array(
                    $this->Html->link(
                        '<i class="fa fa-wrench"></i> Exporter',
                        '/admin/form_manager/forms/export/' . $this->data['Form']['id'],
                        array(
                            'class'             => 'btn btn-success btn-xs',
                            'escape'            => false
                        )
                    )
                )
            )
        );
        ?>
        <div class="widget-body no-padding">

            <?php echo $this->Smart->create('Form', array('class' => 'smart-form'));?>

            <fieldset>

                <?php
                echo $this->Smart->input('Form.name', 'Nom : ');
                echo $this->Smart->hidden('Form.user_id');
                echo $this->Smart->hidden('Form.id');
                ?>

            </fieldset>

            <footer>
                <?php echo $this->Smart->submit();?>
            </footer>

            <?php
            echo $this->Smart->end();
            ?>
        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>

    <article class="col-sm-12 col-md-12 col-lg-8">

        <?php
        echo $this->Smart->openBox(
            array(
                'icone' => 'fa-edit',
                'titre' => 'Contenu du formulaire',
                'btns'  => array(
                    $this->Html->link(
                        '<i class="fa fa-plus-square"></i> Ajouter un élément',
                        '/admin/form_manager/forms/add_element/' . $this->data['Form']['id'],
                        array(
                            'class'             => 'btn btn-success btn-xs',
                            'data-toggle'       => 'modal',
                            'data-target'       => '#myModal',
                            'escape'            => false
                        )
                    )
                )
            )
        );
        ?>
        <div class="widget-body no-padding" id="elements">



        </div>
        <?php echo $this->Smart->closeBox();?>

    </article>
</div>

<?php $this->Html->scriptStart(array('inline' => false));?>

    /**
     * Get the form ID
     */
    var formId = <?=$this->data['Form']['id'];?>;

    /**
     * Load the elements of a form
     */
    function loadElements(formId)
    {
        if (formId == undefined) {

            console.log('Form Id undefined');
            return;
        }

        /**
         * Load the elements in the div
         */
        $('#elements').load('/admin/form_manager/form_elements/list/' + formId);
    }

    loadElements(formId);

<?php $this->Html->scriptEnd();?>
