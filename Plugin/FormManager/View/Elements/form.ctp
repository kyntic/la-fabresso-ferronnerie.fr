<?php

/**
 * Get the content
 */
$formContent = $this->requestAction('/form_manager/forms/display/' . $pageId);
?>

<div class="row page-row" id="my-form-<?=$formContent['form']['Form']['id'];?>">
    <div class="contenuPage col-md-8 col-sm-7">
        <?php if ($formContent) : ?>
            <h2 class="title"><?=$formContent['form']['Form']['name'];?></h2>
            <?=$this->Form->create('Form', array('class' => 'contact-form', 'url' => '/form_manager/forms/submit'));?>

            <?=$this->Form->hidden('Form.id', array('value' => $formContent['form']['Form']['id']));?>
            <?=$this->Form->hidden('Form.redirection', array('value' => $redirectionPage));?>

            <?=$this->FrontForm->displayFormManagerElements($formContent['formElements']);?>

            <div class="form-action">
                <?=$this->Form->button('Envoyer <i class="fa fa-chevron-right"></i>', array('class' => 'btn btn-theme', 'escape' => false, 'type' => 'submit'));?>
            </div>

            <?=$this->Form->end();?>
        <?php endif; ?>
    </div>
</div>

<?php $this->Html->scriptStart(array('inline' => false));?>
//<script type="text/javascript">

    /**
     * Form submit action
     */
    $('#FormViewForm').on('submit', function(e) {

        e.preventDefault();

        var form = $(this);

        var formId = <?=$formContent['form']['Form']['id'];?>;

        $.ajax({
            type 		: 'POST',
            url 		: $(this).attr('action'),
            data 		: $(this).serialize(),
            cache 	    : false,
            complete	: function() {

                $(form).hideLoading()
            },
            success 	: function(response) {

                /**
                 * Get the response
                 */
                var response = jQuery.parseJSON(response);

                if (response.status == 0) {

                    /**if (response.errors != undefined) {

                        $('.input').removeClass('state-error');
                        $('.note-error').remove();

                        $.each(response.errors, function(index, value) {

                            var alert = '<div class="note note-error">' + value + '</div>';

                            $('input[name="data[FormElement][configuration][' + index + ']"]').parent().addClass('state-error');
                            $('input[name="data[FormElement][configuration][' + index + ']"]').after(alert);
                        });
                    }**/
                }

                if (response.status == 1) { //Ok

                    alert('Formulaire envoyé, merci.');

                    $('#my-form-' + formId).remove();
                }
            }
        });
    });

<?php $this->Html->scriptEnd();?>