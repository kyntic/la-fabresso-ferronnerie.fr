<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $index;?>">
                <?=$content['content']->title;?>
            </a>
        </h4>
    </div>
    <div id="collapse-<?php echo $index;?>" class="panel-collapse collapse">
        <div class="panel-body">
            <?=$content['content']->txt;?>
        </div>
    </div>
</div>