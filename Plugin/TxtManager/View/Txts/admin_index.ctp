<?php 
$this->assign('titre', '<i class="icon-file-alt"></i> Liste des textes');
$this->Html->addCrumb(__('Gestion des textes'), $this->here);

echo $this->Smart->openBox(
    array(
        'icone' => 'fa-list', 
        'btns'  => ''
    )
);
?>
<div class="widget-body no-padding WhMenu">

    <table class="table table-normal table-striped">

        <thead>

            <tr>
                <th class="col-md-1">Admin</th>
                <th class="col-md-1">ID</th>
                <th class="col-md-1">Description</th>
            </tr>

        </thead>

        <tbody>

            <?php foreach($Txts as $v) : ?>

                <tr>
                    <td class="col-md-1"><?php echo $this->Html->link('<i class="fa fa-edit"></i>', '/admin/txt_manager/txts/edit/'.$v['Txt']['id'], array('escape' => false, 'class' => 'btn btn-primary btn-xs')); ?></td>
                    <td class="col-md-1"><?php echo $v['Txt']['id']; ?></td>
                    <td class="col-md-1"><?php echo $v['Txt']['description']; ?></td>
                </tr>

            <?php endforeach;?>

        </tbody>

    </table>

</div>
<?php $this->Smart->closeBox(); ?>