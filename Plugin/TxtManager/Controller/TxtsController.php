<?php

class TxtsController extends TxtManagerAppController {
   
	public function admin_index () {

		$Txts = $this->Txt->find('all');
		if(!$Txts) $Txts = array();
		$this->set(compact('Txts'));

		$this->set('MenuAdminActives', array(30, 117));

	}

	public function admin_edit ($id = null) {


		if(!empty($this->request->data)) {

			$data = $this->request->data;

			$this->Txt->save($data);

			$this->Session->setFlash('Texte mis à jour' , 'success');

			$this->redirect('/admin/txt_manager/txts');

		}elseif($id) {

			$this->data = $this->Txt->findById($id);

		}

		$this->set('MenuAdminActives', array(30, 117));

	}

}