<?php if(!empty($evenements)){?>
    <section class="events">

        <h3 class="title">Nos évènements à venir</h3>

        <div class="section-content">
            <?php foreach($evenements as $k => $v) : ?>
                <article class="events-item row page-row">
                    <div class="date-label-wrapper col-md-3 col-sm-4 col-xs-4">
                        <p class="date-label">
                            <span class="month"><?php echo $this->WhDate->show($v['Even']['even_date_deb'],'mois_court');?></span>
                            <span class="date-number"><?php echo $this->WhDate->show($v['Even']['even_date_deb'],'jour');?></span>
                        </p>
                    </div><!--//date-label-wrapper-->
                    <div class="details col-md-9 col-sm-8 col-xs-8">
                        <h5 class="title"><?=$this->Html->link($v['Even']['name'], $v['Even']['url']);?></h5>
                        <p class="time text-muted"><i class="fa fa-clock-o"></i> <?=$this->WhDate->show($v['Even']['even_date_deb'],'heure');?> - <?php echo $this->WhDate->show($v['Even']['even_date_fin'],'heure');?></p>
                        <p class="time text-muted"><i class="fa fa-map-marker"></i> <?php echo $v['Even']['adresse'];?><br /></p>
                        <p class="time text-muted"><i class="glyphicon glyphicon-info-sign"></i> <?php echo $v['Even']['resume'];?></p>

                    </div><!--//details-->
                </article>
            <?php endforeach;?>
        </div>
    </section>
<?php }?>
