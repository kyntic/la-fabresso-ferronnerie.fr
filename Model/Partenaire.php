<?php
class Partenaire extends AppModel {

    public $useTable = 'actualites';

    public $belongsTo = array(
        'ContentType',
        'Etat'
    );

    /**
     * Before Save
     * Modificaiton du format de la date
     * @return bool
     */
    public function beforeSave($options = Array()) {

        if(empty($this->data[$this->alias]['name']) && !empty($this->data[$this->alias]['h1'])) {

            $this->data[$this->alias]['name'] = $this->data[$this->alias]['h1'];

        }

        if(!empty($this->data[$this->alias]['vignette'])) $this->data[$this->alias]['vignette'] = json_encode($this->data[$this->alias]['vignette']);

        if(!empty($this->data[$this->alias]['id'])) $this->data[$this->alias]['type'] = 'partenaire';

        return true;

    }

    /**
     * After Find :
     * Création d'une url si inexistance
     * @param mixed $results
     * @return mixed
     */
    public function afterFind($results, $primary = false) {

    	foreach ($results as &$v) {

            if (!empty($v[$this->alias])) {

                if (!empty($v[$this->alias]['vignette'])) {

                    $v[$this->alias]['vignette'] = json_decode($v[$this->alias]['vignette']);

                }

            }
        }

        return $results;


    }



}
