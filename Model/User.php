<?php

/**
 * Class User
 */
class User extends AppModel
{
    /**
     * The belongs to
     *
     * @var array
     */
    public $belongsTo = array(
        'Group'
    );

    /**
     * The validation
     *
     * @var array
     */
    public $validate = array(
        'nom' => array(
            'nonVide' => array(
                'rule'          => 'notEmpty',
                'required'      => true,
                'allowEmpty'    => false,
                'message'       => 'Veuillez renseigner un nom'
            )
        ),
        'prenom' => array(
            'nonVide' => array(
                'rule'          => 'notEmpty',
                'required'      => true,
                'allowEmpty'    => false,
                'message'       => 'Veuillez renseigner un prénom'
            )
        ),        
        'motdepasse' => array(
            'tailleMin' => array(
                'rule'     => array('minLength', '8'),
                'allowEmpty' => false, 
                'message'  => '8 caractères minimum'
            )
        ),         
        'email' => array(    
            'regle-0' => array(
                'rule'          => 'notEmpty',
                'required'      => true,
                'allowEmpty'    => false,
                'message'       => 'Veuillez renseigner un email'
            ),              
            'regle-1' => array(
                'rule'          => array('email', true),
                'message'       => 'Veuillez renseigner un email valide'
            ),
            'regle-2' => array(
                'rule'    => 'isUnique', 
                'allowEmpty' => false, 
                'message' => 'Cet email est déjà utilisé'
            )
        ), 
        'confirm_password' => array(
            'verification' => array(
                'rule' => array('compareToField', 'motdepasse'),
                'message' => 'Le mot de passe et sa confirmation sont différents'
            )
        )
    );  


    public function beforeSave($options = array()) {

        if(isset($this->data['User']['motdepasse'])) {

            $this->data['User']['password'] = AuthComponent::password($this->data['User']['motdepasse']);

        }

        if(isset($this->data['User']['prenom'])) {

            $this->data['User']['nom_connexion'] = $this->data['User']['prenom']. ' '. $this->data['User']['nom'];

        }

        if(!empty($this->data[$this->alias]['vignette']) && is_array($this->data[$this->alias]['vignette'])) $this->data[$this->alias]['vignette'] = json_encode($this->data[$this->alias]['vignette']);


        return true;
        
    }   

    /**
     * AfterFind
     */
    public function afterFind($results, $primary = false) {
        
        foreach ($results as $k => $v) {
            
            if (isset($v['User']['nom_connexion']) && empty($v['User']['nom_connexion'])) $results[$k]['User']['nom_connexion'] = $v['User']['email'];

            if (!empty($v[$this->alias]['vignette'])) {

                $v[$this->alias]['vignette'] = json_decode($v[$this->alias]['vignette']);

            }

        }

        return $results;

    }


    /**
     * Permet de vérifier si les deux mots de passe saisie sont bien idnetiques
     */
    protected function compareToField( $field = array(), $compared_field = null )
    {
        foreach( $field as $v){
     
            $current_field = $v;

            $compared_field = $this->data[$this->name][ $compared_field ];

            if($current_field !== $compared_field) {

                return false;

            }
        }
    
        return true;

    }

    /**
     * Genere un mot de passe
     */
    public function genere_mot_de_passe($length)
    {
        $src = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9');
        
        $mot_de_passe = '';
        
        for($i = 0; $i < $length; $i++) {
        
            $mot_de_passe .= $src[rand(0, 30)];
        
        }
        
        return $mot_de_passe;
    }

    public function envoyer_compte ($id) {

        $user = $this->findById($id);

        $Txt = ClassRegistry::init('TxtManager.Txt');
        $txts = $Txt->prepare(array('compte_inscription'), $user);

        $Parametre = ClassRegistry::init('Parametre');
        $parametres = $Parametre->find('first');

        App::uses('CakeEmail', 'Network/Email');

        $email = new CakeEmail();
        $email->emailFormat('html');
        $email->from($parametres['Parametre']['email_noreply']);
        $email->to($user['User']['email']);

        $email->subject(Configure::read('Projet.nom').' : Inscription');

        $vars = array();
        $vars['txts'] = $txts;

        $email->viewVars($vars);
        $email->template('compte_inscription');

        return $email->send();

    }

    public function envoyer_compte_perdu ($id) {

        $user = $this->findById($id);

        $motdepasse = $this->genere_mot_de_passe(8);
        $password = AuthComponent::password($motdepasse);

        $user['User']['motdepasse'] = $motdepasse;
        $user['User']['password'] = $password;

        $this->save($user, array('validate' => false));

        $Parametre = ClassRegistry::init('Parametre');
        $parametres = $Parametre->find('first');

        App::uses('CakeEmail', 'Network/Email');

        $email = new CakeEmail();
        $email->emailFormat('html');

        $email->from($parametres['Parametre']['email_noreply']);
        $email->to($user['User']['email']);

        $email->subject(Configure::read('Projet.nom').' : Mot de passe perdu');


        $email->viewVars($user);
        $email->template('compte_perdu');

        return $email->send();

    }

}