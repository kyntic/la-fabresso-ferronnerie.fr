<?php

/**
 * Class AdminMenu
 */
class AdminMenu extends AppModel
{
    /**
     * The belongs to
     *
     * @var array $belongsTo
     */
    public $belongsTo = array('Group');

    /**
     * The Act as
     *
     * @var array $actAs
     */
    public $actsAs = array('Tree');
}