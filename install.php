<?php

	$defaultFiles = array(
		array(
			'name'          =>  'core.php',
			'defaultName'   =>  'core.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  'database.php',
			'defaultName'   =>  'database.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  'email.php',
			'defaultName'   =>  'email.default.php',
			'folder'        =>  'Config/'
		),
		array(
			'name'          =>  '.htaccess',
			'defaultName'   =>  '.htaccess.default',
			'folder'        =>  'htdocs/'
		)
	);

	foreach ($defaultFiles as $defaultFile) {

		copy(getcwd() . '/' . $defaultFile['folder'] . $defaultFile['defaultName'], getcwd() . '/' . $defaultFile['folder'] . $defaultFile['name']);

	}
