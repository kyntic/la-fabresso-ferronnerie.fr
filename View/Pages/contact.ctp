
<?php
$this->element('balises_metas', array('metas' => $page['Page']));?>

<section class="inner-intro bg-img23 light-color overlay-dark parallax parallax-background2" style="background-position: 0% 0px;">
    <div class="container">
        <div class="row title">
            <h2 class="h2"><?=$page['Page']['h1'];?></h2>
           
        </div>
    </div>
</section>



    <!-- Theme CSS and JS Files files -->
    <div class="full-width-container white-bg">
        <div class="container top50 padding-top48 padding-bottom24 white-bg">
            <div class="row">
                <div class="col-md-4 padding-top24 ">
                    <h3>Nous contacter :</h3>
                    <?php

                    echo $this->Form->create('Contact', array('url' => '/contact_manager/contacts/index', 'id' => 'ContactIndexForm', 'class' => 'contact-form margin-top24'));
                    echo $this->Form->input('nom', array('data-name' => 'nom', 'placeholder' => 'Nom', 'label' => false, 'div' => false));

                    echo $this->Form->input('prenom', array('data-name' => 'prenom', 'placeholder' => 'Prénom', 'label' => false, 'div' => false));
                    echo $this->Form->input('email', array('data-name' => 'email', 'placeholder' => 'Email', 'label' => false, 'div' => false));
                    echo $this->Form->input('sujet', array('data-name' => 'sujet', 'placeholder' => 'Sujet', 'label' => false, 'div' => false));
                    echo $this->Form->input('message', array('data-name' => 'message', 'placeholder' => 'Message', 'label' => false, 'div' => false, 'type' => 'textarea', 'rows' => 9));
                    ?>
                    <button type="submit" class="btn contact-btn pull-right">Envoyer</button>
                    <?php echo $this->Form->end();?>

                </div>
                <div class="col-md-8 padding-top48 ">
                    <?=Configure::read('Params.map');?>
                    <div class="padding-top48">
                                                <div class="margin-bottom12"><h1><?=$page['Page']['sous_titre'];?></h1></div>

                        <p class="text-indent margin-top12"><?=$page['Page']['body'];?></div></p>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php
if (!empty($ContentModules)) {

    foreach ($ContentModules as $k => $module) {

        switch ($module['ContentModule']['module']) {

            default :

                echo $this->element(
                    'ModuleManager.' . $module['ContentModule']['module'],
                    array(
                        'page' => $page['Page'],
                        'content' => $module['ContentModule'],
                        'classBlock' => ($k%2) ? 'pale-gray-bg' : 'white-bg'
                    )
                );

            break;
        }
    }
}

?>
    </div>
    

<?php
$this->Html->script('loading/jquery.showLoading.min.js', array('block' => 'scriptBottom'));
$this->Html->scriptStart(array('inline' => false));
?>
    $('#ContactIndexForm').on('submit', function (e) {

    var form = $(this);

    $(form).showLoading();
    $('.alert', this).remove();
    $('.error_text', this).remove();

    $.ajax({
    type 		: 'POST',
    url 		: $(this).attr('action'),
    data 		: $(this).serialize(),
    cache 	: false,
    complete	: function () {$(form).hideLoading()},
    success 	: function (reponse) {

    var reponse = jQuery.parseJSON(reponse);

    if(reponse.statut == 0) { //Erreur

    $('input', form).each(function (index) {

    var elt 	= $(this);
    var champ 	= elt.data('name');

    if(reponse['erreurs'][champ]) {

    var text_erreur = '<span class="error_text">' + reponse['erreurs'][champ][0] + '</p></span>';
    elt.after(text_erreur);
    elt.css('background', '#f2dede');

    }

    });

    $('textarea', form).each(function (index) {

    var elt 	= $(this);
    var champ 	= elt.data('name');

    if(reponse['erreurs'][champ]) {

    var text_erreur = '<span class="error_text">' + reponse['erreurs'][champ][0] + '</p></span>';
    elt.after(text_erreur);
    elt.css('background', '#f2dede');

    }

    });

    var alert = '<div class="alert alert-error"><h3 class="alert-heading">'+reponse.message.titre+'</h3><p>'+reponse.message.texte+'</p></div>';
    form.prepend(alert);


    }

    if(reponse.statut == 1) { //ok

    form.html('<div class="alert alert-success"><h3 class="alert-heading">'+reponse.message.titre+'</h3><p>'+reponse.message.texte+'</p></div>');

    }

    }

    });

    return false;

    });



<?php
$this->Html->scriptEnd();

$this->end();
?>