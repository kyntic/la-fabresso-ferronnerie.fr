<?php
$this->extend('/Templates/template_default');
$this->element('balises_metas', array('metas' => $page['Page']));

$this->assign('h1', $page['Page']['h1']);


foreach($_breadcrumb['Chemin'] as $v) $this->Html->addCrumb($v[$_breadcrumb['Model']]['name'], $v[$_breadcrumb['Model']]['url']);

$this->start('body');
?>
/view/Pages/home_real.ctp
    <div class="full-width-container white-bg gray-border-top gray-border-bott" id="zone_metier">

        <div class="container padding-top48 padding-bottom96 text-center">


            <h2><?=$page['Page']['sous_titre'];?></h2>
            <p class="padding-bottom96"><?=$page['Page']['body'];?></p>

            <div class="container  padding-bottom24">
                <div id="options" class="clearfix">
                    <div class="option-combo">
                        <ul id="filter" class="margin0 no-style font-dosis font600 home-page-qsand-select margin-bottom12 pull-left padding0 option-set clearfix" data-option-key="filter">
                            <li><a href="#show-all" data-option-value="*" class="selected">ALL</a></li>
                            <?php foreach($Reals as $v) : ;?>
                            <li><a href="#projet_type_<?=$v['Page']['id'];?>" data-option-value=".projet_type_<?=$v['Page']['id'];?>"><?=$v['Page']['name'];?></a></li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
                <ul id="container" class="no-style margin0 q-sand-target four-columns-q-sand padding0 super-list variable-sizes clearfix isotope">
                    <?php foreach($Refs as $v) : ;?>
                    <li class="pull-left border-box relative overflow-hidden element * projet_type_<?=$v['Actualite']['page_id'];?> isotope-item">
                        <?=$this->element('bloc_real', array('v' => $v));?>
                    </li>
                <?php endforeach;?>

                </ul>

            </div>

        </div>

    </div>
<script type="text/javascript" src="<?=WH_ROOT;?>/themeforest/Site/js/jquery.isotope.min.js"></script>
<script>



    (function($){
        $(document).ready(function(){
            var $container = $('#container');
            $container.imagesLoaded(function() {
                $container.isotope({
                    masonry: {
                        columnWidth: 1
                    },
                    sortBy: 'original-order',
                    getSortData: {
                        number: function( $elem ) {
                            var number = $elem.hasClass('element') ?
                                $elem.find('.number').text() :
                                $elem.attr('data-number');
                            return parseInt( number, 10 );
                        },
                        alphabetical: function( $elem ) {
                            var name = $elem.find('.name'),
                                itemText = name.length ? name : $elem;
                            return itemText.text();
                        }
                    }
                });

            });

            var $optionSets = $('#options .option-set'),
                $optionLinks = $optionSets.find('a');

            $optionLinks.click(function(){
                var $this = $(this);
                // don't proceed if already selected
                if ( $this.hasClass('selected') ) {
                    return false;
                }
                var $optionSet = $this.parents('.option-set');
                $optionSet.find('.selected').removeClass('selected');
                $this.addClass('selected');

                // make option object dynamically, i.e. { filter: '.my-filter-class' }
                var options = {},
                    key = $optionSet.attr('data-option-key'),
                    value = $this.attr('data-option-value');
                // parse 'false' as false boolean
                value = value === 'false' ? false : value;
                options[ key ] = value;
                if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
                    // changes in layout modes need extra logic
                    changeLayoutMode( $this, options )
                } else {
                    // otherwise, apply new options
                    $container.isotope( options );
                }
                return false;
            });
        });
    })(jQuery);
</script>
<?php
$this->end();
?>
