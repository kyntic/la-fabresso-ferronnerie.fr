      
        <!-- Service Section -->
        <section id="service" class="ptb ptb-sm-80">
            <div class="wow fadeInLeft container text-center">
                <p class="services">Nos services de ferronnerie</h3>
                <div class="row">
                    
                    <div class="col-md-3 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/6"><img src="/mazel/img/services/grilles.png"></a></div>
                        <p class="serv"><a href="/pages/view/6">Grilles</a></p>
                    </div>
                    <div class="col-md-3 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/7"><img src="/mazel/img/services/mains.png"></a></div>
                        <p class="serv"><a href="/pages/view/7">Mains courantes</a></p>
                    </div>
                    <div class="col-md-3 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/8"><img src="/mazel/img/services/tonnelles.png"></a></div>
                        <p class="serv"><a href="/pages/view/8">Tonnelles</a></p>
                    </div>
                    <div class="col-md-3 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/9"><img src="/mazel/img/services/verandas.png"></a></div>
                        <p class="serv"><a href="/pages/view/9">Vérandas</a></p>
                    </div>
                    <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/10"><img src="/mazel/img/services/pergolas.png"></a></div>
                        <p class="serv"><a href="/pages/view/10">Pergolas</a></p>
                    </div>
					  <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/11"><img src="/mazel/img/services/portes.png"></a></div>
                        <p class="serv"><a href="/pages/view/11">Portes/Portails</a></p>
                    </div>
					  <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/12"><img src="/mazel/img/services/garde-corps.png"></a></div>
                        <p class="serv"><a href="/pages/view/12">Garde-corps</a></p>
                    </div>
					
                </div>
				<div class="wow fadeInLeft container text-center">
				<p class="services">Nos services de ferronnerie d'art</h3>
				 <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/13"><img src="/mazel/img/services/mobilier.png"></a></div>
                        <p class="serv"><a href="/pages/view/13">Mobilier</a></p>
                    </div>
					 <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/14"><img src="/mazel/img/services/tringles.png"></a></div>
                        <p class="serv"><a href="/pages/view/14">Tringles à rideaux</a></p>
                    </div>
					 <div class="col-md-4 col-sm-6 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/15"><img src="/mazel/img/services/rampes.png"></a></div>
                        <p class="serv"><a href="/pages/view/15">Rampes/Escaliers</a></p>
                    </div>
					 <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-0 mb-45">
                        <div class="page-icon-top"><a href="/pages/view/16"><img src="/mazel/img/services/objets.png"></a></div>
                        <p class="serv"><a href="/pages/view/16">Objets/Sculptures</a></p>
                    </div>
				
				</div>
            </div>
        </section>
        <!-- End Service Section -->