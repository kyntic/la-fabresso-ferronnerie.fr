
        <!-- Work Section -->
        <section id="work" class="wow fadeIn pt pt-sm-80 text-center">
            <div class="container">
                <h3>Great Work</h3>
                <div class="spacer-30"></div>
                <!-- work Filter -->
                <div class="row">
                    <ul class="container-filter categories-filter">
                        <li><a class="categories active" data-filter="*">All</a></li>
                        <li><a class="categories" data-filter=".branding">Branding</a></li>
                        <li><a class="categories" data-filter=".design">Design</a></li>
                        <li><a class="categories" data-filter=".photo">Photo</a></li>
                        <li><a class="categories" data-filter=".coffee">coffee</a></li>
                    </ul>
                </div>
                <!-- End work Filter -->
            </div>
            <!-- Work Gallary -->
            <div class="container-fluid ">
                <div class="row">
                    <div class="container-grid nf-col-4">

                        <div class="nf-item branding design coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/1.jpg" title="Consequat massa quis">
                                    <img class="item-container" src="/mazel/img/portfolio/1.jpg" alt="1" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Consequat massa quis</h5>
                                            <p class="white">Branding, Design, Coffee</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item photo">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/2.jpg" title="Vivamus elementum semper">
                                    <img class="item-container" src="/mazel/img/portfolio/2.jpg" alt="2" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Vivamus elementum semper</h5>
                                            <p class="white">Photo</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/3.jpg" title="Quisque rutrum">
                                    <img class="item-container" src="/mazel/img/portfolio/3.jpg" alt="4" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Quisque rutrum</h5>
                                            <p class="white">Branding, Design, Coffee</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/4.jpg" title="Tellus eget condimentum">
                                    <img class="item-container" src="/mazel/img/portfolio/4.jpg" alt="4" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Tellus eget condimentum</h5>
                                            <p class="white">Design</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item branding design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/5.jpg" title="Nullam quis ant">
                                    <img class="item-container" src="/mazel/img/portfolio/5.jpg" alt="5" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Nullam quis ant</h5>
                                            <p class="white">Branding, Design</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item photo">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/6.jpg" title="Sed fringilla mauris">
                                    <img class="item-container" src="/mazel/img/portfolio/6.jpg" alt="6" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Sed fringilla mauris</h5>
                                            <p class="white">Photo</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item design">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/7.jpg" title="Augue velit cursus">
                                    <img class="item-container" src="/mazel/img/portfolio/7.jpg" alt="7" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Augue velit cursus</h5>
                                            <p class="white">Design</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="nf-item coffee">
                            <div class="item-box">
                                <a class="cbox-gallary1" href="img/portfolio/8.jpg" title="Donec sodales sagittis">
                                    <img class="item-container" src="/mazel/img/portfolio/8.jpg" alt="8" />
                                    <div class="item-mask">
                                        <div class="item-caption">
                                            <h5 class="white">Donec sodales sagittis</h5>
                                            <p class="white">Coffee</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Work Gallary -->
        </section>
        <!-- End Work Section -->
        
        <!-- Action Box Section -->
        <section id="action-box" class="dark-bg ptb-60">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h4 class="">Penatibus magnis dis parturient?</h4>
                        <p class="mb-0">Porttitor eu consequat vitae 9hasellus viverra nulla ut metus varius laoreet vitae.</p>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <a class="btn btn-md btn-white float-right float-none-xs">See All Work</a>

                    </div>
                </div>
            </div>
        </section>
        <!-- End Action Box Section -->
