<!DOCTYPE html>
<html>
<head>

    <?php
        echo $this->Html->charset();
        echo '<title>' . $this->fetch('meta_title') . '</title>';
        echo $this->Html->meta('description', $this->fetch('meta_description'));
        echo $this->Html->meta('keywords', $this->fetch('meta_keywords'));
        echo $this->Html->meta(array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1'));
        echo $this->Html->meta(array('name' => 'author', 'content' => 'Whatson Web'));
        echo $this->Html->meta(array('name' => 'robots', 'content' => $this->fetch('meta_robots')));
    ?>


    <!-- Favicone Icon -->

    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <link rel="apple-touch-icon" href="img/favicon.png">

    <!-- CSS TEMPLATE -->
    <?= $this->Html->css('/mazel/css/style.css'); ?>
    <?= $this->Html->css('/mazel/css/bootstrap.css'); ?>
    <?= $this->Html->css('/mazel/css/font-awesome.css'); ?>
    <?= $this->Html->css('/mazel/css/ionicons.css'); ?>
    <?= $this->Html->css('/mazel/css/plugin/jPushMenu.css'); ?>
    <?= $this->Html->css('/mazel/css/plugin/animate.css'); ?>
    <?= $this->Html->css('/mazel/css/jquery-ui.css'); ?>

    <!-- CSS PERSO -->
    <?= $this->Html->css('/mazel/css/corrections.css'); ?>


</head>

<body class="full-intro">

    <!-- Preloader -->
    <section id="preloader">
        <div class="loader" id="loader">
            <div class="loader-img"></div>
        </div>
    </section>
    <!-- End Preloader -->

    <!-- Sidemenu -->
    <section class="side-menu cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right">
        <a class="menu-close toggle-menu menu-right push-body"><i class="ion ion-android-close"></i></a>
        <h5 class="white">Sign In</h5>
        <div class="sign-in">
            <input class="input-sm form-full" type="email" aria-required="true" id="email" name="email" placeholder="Email" value="" />
            <input class="input-sm form-full" type="password" aria-required="true" id="password" name="password" placeholder="Password" value="" />
            <input type="submit" class="btn btn-md btn-color-b form-full" value="Sign In" />
            <a>New Customer?</a>
        </div>
        <ul>
            <li><a target="_blank" href="index.html">Main Demo Page</a></li>
            <li><a href="home.html">Home Default</a></li>
            <li><a href="portfolio-grid-2col.html">Portfolio</a></li>
            <li><a href="login-register.html">Login & Signup</a></li>
            <li><a href="faq-1.html">FAQ</a></li>
            <li><a href="about-1.html">About</a></li>
            <li><a href="service-1.html">Service</a></li>
            <li><a href="blog-grid-3col.html">Blog</a></li>
            <li><a href="404-error-1.html">404 Error</a></li>
            <li><a href="shop-checkout.html">Shopping Cart</a></li>
            <li><a href="contact-1.html">Contact Us</a></li>
        </ul>
    </section>
    <!--End Sidemenu -->

    <!-- Search menu Top -->
    <section class=" top-search-bar cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top">
        <div class="container">
            <div class="search-wraper">
                <input type="text" class="input-sm form-full" placeholder="Search..." name="search" />
                <a class="search-bar-icon"><i class="fa fa-search"></i></a>
                <a class="bar-close toggle-menu menu-top push-body"><i class="ion ion-android-close"></i></a>
            </div>
        </div>
    </section>
    <!--End Search menu Top -->

    <!-- Site Wraper -->
    <div class="wrapper">

        <!-- HEADER -->
        <header class="header">
            <div class="container">

                <!-- logo -->
                <div class="logo">
                    <a href="home.html">
                        <img class="l-black" src="/mazel/img/logo-black.png" />
                        <img class="l-white" src="/mazel/img/logo-white.png" />
                        <img class="l-color" src="/mazel/img/logo-color.png" />
                    </a>
                </div>
                <!--End logo-->

                <!-- Rightside Menu (Search, Cart, Bart icon) -->
                <div class="side-menu-btn">
                    <ul>
                        <!-- Search Icon -->
                        <li class="">
                            <a class="right-icon search toggle-menu menu-top push-body"><i class="fa fa-search"></i></a>
                        </li>
                        <!-- End Search Icon -->

                        <!-- Cart Icon -->
                        <li class="">
                            <a href="shop-checkout.html" class="right-icon cart">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="cart-notification">2</span>
                            </a>
                            <!-- Cart Dropdown List -->
                            <div class="cart-dropdown">
                                <ul class="cart-list">
                                    <li class="light-color">
                                        <a class="close-cart-list"><i class="fa fa-times-circle"></i></a>
                                        <div class="media">
                                            <a class="pull-left">
                                                <img src="/mazel/img/product/02.jpg" alt="fashion" /></a>
                                            <div class="media-body">
                                                <h6><a>Fashion Model New /01</a></h6>
                                                <p>$399.00</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="light-color">
                                        <a class="close-cart-list"><i class="fa fa-times-circle"></i></a>
                                        <div class="media">
                                            <a class="pull-left">
                                                <img src="/mazel/img/product/06.jpg" alt="fashion" /></a>
                                            <div class="media-body">
                                                <h6><a>Fashion Model New /02</a></h6>
                                                <p>$399.00</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <p class="cart-sub-totle"><span class="pull-left">Cart Subtotal</span><span class="pull-right totle"><strong>$798.00</strong></span></p>
                                <div class="clearfix"></div>
                                <div class="cart-checkout-btn">
                                    <a href="shop-checkout.html" class="btn btn-md btn-color-b form-full checkout">Checkout</a>
                                </div>
                            </div>
                            <!-- End Cart Dropdown List -->
                        </li>
                        <!-- End Cart Icon -->

                        <!--Sidebar Menu Icon-->
                        <li class="">
                            <a class="right-icon bar-icon toggle-menu menu-right push-body"><i class="fa fa-bars"></i></a>
                        </li>
                        <!-- End Sidebar Menu Icon-->
                    </ul>
                </div>
                <!-- End Rightside Menu -->

                <!-- Navigation Menu -->
                <nav class='navigation'>

                                            <?php
                                            if(!empty($_menuPages)) {
                                                $this->WhTree->html = '';
                                                $this->WhTree->Model = 'Page';
                                                $this->WhTree->niveau = 0;
                                                echo $this->WhTree->generate_nav_simple($_menuPages);
                                            }
                                            ?>
                </nav>
                <!--End Navigation Menu -->

            </div>
        </header>
        <!-- END HEADER -->

        <?= $this->fetch('content'); ?>

        <!-- FOOTER -->
        <footer class="footer pt-80">
            <div class="container">
                <div class="row mb-60">
                    <!-- Logo -->
                    <div class="col-md-7 col-sm-8 col-xs-12 mb-xs-30">
                        <li><?php echo $this->Html->link($this->Html->image('logo.png'), FULL_BASE_URL, array('escape' => false)); ?></li>
                    </div>
                    <!-- Logo -->

                    

                    <!-- Social -->
                    <div class="col-md-3 col-md-offset-2 col-sm-4 col-xs-12">
                        <ul class="social">
                            <li><a target="_blank" href="https://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                            <li><a target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_blank" href="https://vimeo.com/"><i class="fa fa-vimeo-square"></i></a></li>
                            <li><a target="_blank" href="https://www.behance.net/"><i class="fa fa-behance"></i></a></li>
                        </ul>
                    </div>
                    <!-- End Social -->
                </div>
                <!--Footer Info -->
                <div class="row footer-info mb-60">
                    <div class="col-md-3 col-sm-12 col-xs-12 mb-sm-30">
                         <div class="about-footer-txt padding-top12"><?=nl2br(Configure::read('Params.about'));?></div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 mb-sm-30 mb-xs-0">
                          <ul class="margin0 no-style footer-nav padding0">
                            <?php
                            foreach($_menuPages as $v) : ;
                                echo '<li>'.$this->Html->link($v['Page']['name'], $v['Page']['url']).'</li>';
                            endforeach;
                            ?>
                        </ul>
                    </div>
                   
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <li><?=Configure::read('Params.adresse_1');?> <?=Configure::read('Params.cp');?>  <?=Configure::read('Params.ville');?></li>
                            <li>Tel.: <?=Configure::read('Params.telephone');?></li>
                            <li><?php echo $this->Html->link('Nous contacter', $_lienContact['Page']['url']);?></li>
                            <li><?php echo $this->Html->link('Mentions légales', $_lienMentionsLegales['Page']['url'], array('class' => 'mention_legale')); ?></li>
                            <li>Développement : <?php echo $this->Html->link('Kobaltis','http://www.kobaltis.com', array('class' => 'credit', 'target' => '_blank', 'title' => 'Web agency', 'style' => 'display:inline-block;vertical-align:top;')); ?></li>
                    </div>
                </div>
                <!-- End Footer Info -->
            </div>

            <hr />

            <!-- Copyright Bar -->
            <section class="copyright ptb-60" id="service">
                <div class="container">
                    <p class="">
                        © 2015 <a><b>Mazel Template</b></a>. All Rights Reserved.
                        <br />
                        Template  by <a target="_blank" href="http://nileforest.com/"><b>nileforest</b></a>
                    </p>
                </div>
            </section>
            <!-- End Copyright Bar -->

        </footer>
        <!-- END FOOTER -->

        <!-- Scroll Top -->
        <a class="scroll-top">
            <i class="fa fa-angle-double-up"></i>
        </a>
        <!-- End Scroll Top -->

    </div>
    <!-- Site Wraper End -->


   <!-- JS -->
    <?= $this->Html->script('/mazel/js/jquery-1.11.2.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.easing.js'); ?>
    <?= $this->Html->script('/mazel/js/jquery-ui.min.js'); ?>
    <?= $this->Html->script('/mazel/js/bootstrap.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.flexslider.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/background-check.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.fitvids.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.viewportchecker.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.stellar.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/wow.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.colorbox-min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/owl.carousel.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/isotope.pkgd.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/masonry.pkgd.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jPushMenu.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/jquery.fs.tipper.min.js'); ?>
    <?= $this->Html->script('/mazel/js/plugin/mediaelement-and-player.min.js'); ?>
    <?= $this->Html->script('/mazel/js/theme.js'); ?>
    <?= $this->Html->script('/mazel/js/navigation.js'); ?>



</body>
</html>
