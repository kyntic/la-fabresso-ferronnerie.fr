<header>Modules & contenus de la page</header>
<div id="modules">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Module</th>
            <th>Id</th>
            <th>Nom</th>
            <th>
                <div class="btn-group" style="float:right;z-index: 1;">
                    <button href="#" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-square"></i> Ajouter un module
                    </button>
                    <ul class="dropdown-menu">
                        <li><?= $this->Html->link('<i class="fa fa-list"></i> Formulaire', '/admin/form_manager/forms/add_page/' . $this->data[$Model]['id'] . '/'.$Model, array('data-target' => '#myModal', 'data-toggle' => 'modal', 'escape' => false)); ?></li>
                    </ul>
                </div>
            </th>
        </tr>
        </thead>
        <tbody class="sortable ui-sortable"></tbody>
    </table>
</div>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$('#modules tbody').load('/admin/content_modules/index/<?=$Model;?>/<?=$this->data[$Model]['id'];?>');
<?php
$this->Html->scriptEnd();
?>
