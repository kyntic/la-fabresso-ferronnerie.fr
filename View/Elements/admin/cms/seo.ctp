<header>Référencement</header>
<fieldset>
<?php 
echo $this->Smart->input($Modele.'.meta_title', 'Balise title : Balise essentiel, ne pas dépasser 80 caractères');	
echo $this->Smart->textarea($Modele.'.meta_description', 'Meta Description : ne sert pas au référencement, mais donne une indication aux internautes dans les résultats de recherche ');
echo $this->Smart->textarea($Modele.'.meta_keywords', 'Meta Keywords : Ne sert pas au référencement sur google');
echo $this->Smart->input($Modele.'.meta_robots', 'Robots de recherche : ', array('options' => array('INDEX, FOLLOW' => 'Suivre les liens et indexer', 'NOINDEX, FOLLOW' => 'Ne pas indexer mais suivre les liens', 'INDEX, NOFOLLOW' => 'Indexer mais ne pas suivre les liens', 'NOINDEX, NOFOLLOW' => 'Ne pas indexer et ne pas suivre les liens'), 'class' => 'uniform'));
?>
</fieldset>
		