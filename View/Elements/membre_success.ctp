<div class="alert alert-block alert-success">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Opération réussie</h4>
	<p>
		<?php echo $message ?>
	</p>
</div>