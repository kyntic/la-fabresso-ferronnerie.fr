<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {


	/**
	 * Returns the breadcrumb trail as a sequence of &raquo;-separated links.
	 *
	 * @param string $separator Text to separate crumbs.
	 * @param string $startText This will be the first crumb, if false it defaults to first crumb in array
	 * @param mixed $startPath Url for the first link in the bread crumbs, can be a string or array
	 * @param boolean $escape Escape the content of the link text, set to false if passing an image in
	 * @return string Composed bread crumbs
	 * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/html.html#creating-breadcrumb-trails-with-htmlhelper
	 */
	public function getCrumbsAdmin($separator = '&raquo;', $startText = false, $startPath = '/', $escape = true) {
		
		if (!empty($this->_crumbs)) {
			$out = array();

			if ($startText) {
				
				$out[] = '<li>'.$this->link($startText['text'], $startText['url'], array('escape' => false)).'</li>';      
			
			}


			foreach ($this->_crumbs as $crumb) {

				if (!empty($crumb[1])) {

					$out[] = '<li>'.$this->link($crumb[0], $crumb[1], array('escape' => false)).'</li>';      

				} else {

					$out[] = '<li>'.$this->link($crumb[0], '#', array('escape' => false)).'</li>';

				}
			}

      
      
    
			
			return join($separator, $out);

		} else {

			return null;
		}

	}


	/**
	 * Returns the breadcrumb trail as a sequence of &raquo;-separated links.
	 *
	 * @param string $separator Text to separate crumbs.
	 * @param string $startText This will be the first crumb, if false it defaults to first crumb in array
	 * @param mixed $startPath Url for the first link in the bread crumbs, can be a string or array
	 * @param boolean $escape Escape the content of the link text, set to false if passing an image in
	 * @return string Composed bread crumbs
	 * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/html.html#creating-breadcrumb-trails-with-htmlhelper
	 */
	public function getCrumbsPublic($separator = '&raquo;', $startText = false, $startPath = '/', $escape = true) {

		if (!empty($this->_crumbs)) {
			$out = array();

			if ($startText) {
				
				$out[] = '<li>'.$this->link($startText['text'], $startText['url'], array('escape' => false)).'</li>'; 
			
			}


			foreach ($this->_crumbs as $crumb) {

				$out[] = '<li>'.$this->link($crumb[0].'</li>', $crumb[1], array('escape' => false)).'</li>';

			}

			return join($separator, $out);

		} else {

			return null;
		}

	}



}

