<?php 

$this->extend('/Templates/template_ecommerce');

$this->assign('meta_title', 'Inscription : '.Configure::read('Projet.prefixe_title'));
$this->assign('meta_robots', 'NOINDEX, NOFOLLOW');

$this->start('body');
?>

<div class="row">

	<div class="col-md-6">

		<h2>Vous n'avez pas encore de compte ?</h2>

		<?= $this->Form->create('User', array('url' => '/users/inscription')); ?>
		<?= $this->Form->input('User.nom', array('label' => 'Nom')); ?>
		<?= $this->Form->input('User.prenom', array('label' => 'Prénom')); ?>
		<?= $this->Form->input('User.nom_entreprise', array('label' => 'Entreprise')); ?>
		<?= $this->Form->input('User.email', array('label' => 'Email')); ?>
		<?= $this->Form->input('User.motdepasse', array('label' => 'Mot de passe', 'type' => 'password')); ?>
		<?= $this->Form->input('User.confirm_password', array('label' => 'Confirmation', 'type' => 'password')); ?>

		<div style="text-align:right;" >
			<?= $this->WhForm->submit('M\'inscrire'); ?>
		</div>

		<?= $this->Form->end(); ?>

	</div>

</div>

<?php
$this->end();
?>