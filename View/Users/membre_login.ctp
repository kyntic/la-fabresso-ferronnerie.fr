<?php 
$this->Html->css('/base-admin-3.0/theme/css/pages/signin.css', null, array('inline' => false));
?>
<div class="account-container stacked">
	
	<div class="content clearfix">
		
		<?php echo $this->Form->create('User', array('url' => '/membre/users/login', 'inputDefaults' => array('div' => 'field')));?>
		
			<h1>Identification</h1>		
			
			<div class="login-fields">
				
				<p><?php echo $this->Session->flash('auth');?></p>
				
				<?php 
				echo $this->Form->input('User.email', array('class' => 'form-control input-lg username-field', 'label' => 'Email : ', 'placeholder' => 'Email'));
				echo $this->Form->input('User.password', array('class' => 'form-control input-lg password-field', 'label' => 'Mot de passe : ', 'placeholder' => 'mot de passe'));

				?>		
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
					<?php echo $this->Form->input('Use.keepsession', array('type' => 'checkbox', 'label' => false, 'div' => false, 'class' => 'field login-checkbox'));?>
					<label class="choice" for="Field">Rester connecter</label>
				</span>
				
				<?php echo $this->Form->button('S’identifier', array('class' => 'login-action btn btn-primary'));?>					
				
			</div> <!-- .actions -->
			
		<?php echo $this->Form->end();?>
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	Pas encore de compte ? <a href="/users/inscription">Inscrivez vous</a><br/>
	Mot de passe perdu ? <a href="/users/motdepasseperdu">Cliquez ici</a>
</div> 
